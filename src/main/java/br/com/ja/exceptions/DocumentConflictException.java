package br.com.ja.exceptions;

public class DocumentConflictException extends Exception {

  public DocumentConflictException(String message) {
    super(message);
  }
}
