package br.com.ja;

import br.com.ja.component.data.JAH10DataTransfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class JaAcessoriaApplication {

  @Autowired
  private JAH10DataTransfer data;

  public static void main(String[] args) {
    SpringApplication.run(JaAcessoriaApplication.class, args);
  }


}
