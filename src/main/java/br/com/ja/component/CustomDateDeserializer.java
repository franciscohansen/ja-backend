package br.com.ja.component;

import br.com.ja.util.DateParser;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

@Component
public class CustomDateDeserializer extends StdDeserializer<Date> {

  public CustomDateDeserializer() {
    this(null);
  }

  protected CustomDateDeserializer(Class<?> vc) {
    super(vc);
  }

  @Override
  public Date deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
    try {
      return DateParser.parseDate(p.getText());
    } catch (ParseException e) {
      return null;
    }
  }
}