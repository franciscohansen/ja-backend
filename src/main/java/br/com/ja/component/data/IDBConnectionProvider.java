package br.com.ja.component.data;

import java.sql.Connection;
import java.sql.SQLException;

public interface IDBConnectionProvider {

  Connection h10() throws SQLException;

  Connection ja() throws SQLException;
}
