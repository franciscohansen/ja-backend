package br.com.ja.component.data.transfer;

import br.com.ja.component.data.IDBConnectionProvider;
import br.com.ja.persistence.repositories.ClienteRepository;
import br.com.ja.util.FormatUtils;
import br.com.ja.util.Timer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class ClienteDataTransfer implements IDataTransfer {

  private static final Logger LOG = Logger.getLogger(ClienteDataTransfer.class.getSimpleName());

  @Autowired
  private ClienteRepository repository;


  @Override
  public void download(IDBConnectionProvider provider) {
    String sql = DataTransferConsts.SQL_CLIENTE_DOWNLOAD;
    try (Connection h10 = provider.h10();
         Connection ja = provider.ja();
         PreparedStatement stmt = h10.prepareStatement(sql);
         ResultSet rs = stmt.executeQuery()) {
      while (rs.next()) {
        Timer.<Void>process(() -> {
          try {
            this.jaInsertOrUpdate(rs, ja);
          } catch (SQLException throwables) {
            throwables.printStackTrace();
          }
          return null;
        }, "Insert ou Update Cliente");
      }
    } catch (SQLException e) {
      LOG.log(Level.SEVERE, "Erro baixando Clientes", e);
    }
  }

  private void jaInsertOrUpdate(ResultSet rs, Connection conn) throws SQLException {
    Long idHabil = rs.getLong("id_habil");
    String cpf = rs.getString("cpf_cnpj");
    if (Optional.ofNullable(cpf).orElse("").isEmpty()) {
      return;
    }
    ResultSetMetaData meta = rs.getMetaData();
    LOG.info(meta.getColumnCount() + "");
    boolean bIdHabil = repository.existsByIdHabil(idHabil);
    boolean bCpf = repository.existsByCpfCnpj(FormatUtils.getFormattedDoc(cpf));
    if (bIdHabil || bCpf) {
      String sql = DataTransferConsts.SQL_CLIENTE_DOWNLOAD_UPDATE +
          (bIdHabil ? " where id=?" : " where cpf_cnpj=?");

      try (PreparedStatement stmt = conn.prepareStatement(sql)) {
        ParameterMetaData pMeta = stmt.getParameterMetaData();
        for (int i = 1; i < pMeta.getParameterCount() - 1; i++) {
          stmt.setObject(i, rs.getObject(i + 1), meta.getColumnType(i + 1));
        }
        if (bIdHabil) {
          stmt.setObject(pMeta.getParameterCount(), rs.getObject(1), meta.getColumnType(1));
        } else {
          stmt.setObject(pMeta.getParameterCount(), rs.getObject(5), meta.getColumnType(5));
        }
        LOG.log(Level.INFO, "{0}", stmt.executeUpdate());
      }
    } else {
      String sql = DataTransferConsts.SQL_CLIENTE_DOWNLOAD_INSERT;
      try (PreparedStatement stmt = conn.prepareStatement(sql)) {
        for (int i = 1; i <= meta.getColumnCount(); i++) {
          stmt.setObject(i, rs.getObject(i), meta.getColumnType(i));
        }
        LOG.log(Level.INFO, "{0}", stmt.executeUpdate());
      }
    }
  }

  @Override
  public void upload(IDBConnectionProvider provider) {
    throw new UnsupportedOperationException("Not implemented");
  }
}
