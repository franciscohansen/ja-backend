package br.com.ja.component.data.transfer;

public class DataTransferConsts {

  private DataTransferConsts() {

  }

  public static final String SQL_CLIENTE_DOWNLOAD_UPDATE = "UPDATE cliente\n" +
      "SET deleted=?,\n" +
      "    id_empresa=?,\n" +
      "    id_habil=?,\n" +
      "    cpf_cnpj=?,\n" +
      "    data_nascimento=?,\n" +
      "    email=?,\n" +
      "    endereco_bairro=?,\n" +
      "    endereco_cep=?,\n" +
      "    endereco_complemento=?,\n" +
      "    endereco_logradouro=?,\n" +
      "    endereco_municipio=?,\n" +
      "    endereco_numero=?,\n" +
      "    endereco_pais=?,\n" +
      "    endereco_uf=?,\n" +
      "    endereco_carne_bairro=?,\n" +
      "    endereco_carne_cep=?,\n" +
      "    endereco_carne_complemento=?,\n" +
      "    endereco_carne_logradouro=?,\n" +
      "    endereco_carne_municipio=?,\n" +
      "    endereco_carne_numero=?,\n" +
      "    endereco_carne_pais=?,\n" +
      "    endereco_carne_uf=?,\n" +
      "    estado_civil=?,\n" +
      "    naturalidade=?,\n" +
      "    nome=?,\n" +
      "    nome_mae=?,\n" +
      "    nome_pai=?,\n" +
      "    pessoa_juridica=?,\n" +
      "    razao_social=?,\n" +
      "    rg_ie=?,\n" +
      "    sexo=?,\n" +
      "    id_profissao=?";
  public static final String SQL_CLIENTE_DOWNLOAD = "select p.id,\n" +
      "       p.deleted,\n" +
      "       1                                                             as id_empresa,\n" +
      "       p.id                                                          as id_habil,\n" +
      "       p.cpfcnpj                                                     as cpf_cnpj,\n" +
      "       p.datanascimentofundacao                                      as data_nascimento,\n" +
      "       p.email,\n" +
      "       p.endbairro                                                   as endereco_bairro,\n" +
      "       p.endcep                                                      as endereco_cep,\n" +
      "       p.endcomplemento                                              as endereco_complemento,\n" +
      "       p.endlogradouro                                               as endereco_logradouro,\n" +
      "       p.endmunicipio                                                as endereco_municipio,\n" +
      "       p.endnumero                                                   as endereco_numero,\n" +
      "       p.endpais                                                     as endereco_pais,\n" +
      "       p.enduf                                                       as endereco_uf,\n" +
      "       p.entrbairro                                                  as endereco_carne_bairro,\n" +
      "       p.entrcep                                                     as endereco_carne_cep,\n" +
      "       p.entrcomplemento                                             as endereco_carne_complemento,\n" +
      "       p.entrlogradouro                                              as endereco_carne_logradouro,\n" +
      "       p.entrmunicipio                                               as endereco_carne_municipio,\n" +
      "       p.entrnumero                                                  as endereco_carne_numero,\n" +
      "       p.entrpais                                                    as endereco_carne_pais,\n" +
      "       p.entruf                                                      as endereco_carne_uf,\n" +
      "       p.estadocivil                                                 as estado_civil,\n" +
      "       pc.naturalde                                                  as naturalidade,\n" +
      "       p.nome,\n" +
      "       pf.nomemae                                                    as nome_mae,\n" +
      "       pf.nomepai                                                    as nome_pai,\n" +
      "       (p.tipopessoa = 'JURIDICA' or p.tipopessoa = 'MICRO_EMPRESA') as pessoa_juridica,\n" +
      "       p.rgie                                                        as rg_ie,\n" +
      "       p.sexo,\n" +
      "       null                                                          as id_profissao\n" +
      "from pessoa p\n" +
      "         left join pessoa_cliente pc on p.id = pc.pessoa_id\n" +
      "         left join pessoacliente_filiacao pf on pc.pessoa_id = pf.pessoacliente_pessoa_id\n" +
      "where coalesce(p.cliente, false)\n" +
      "order by p.id;";
  public static final String SQL_CLIENTE_DOWNLOAD_INSERT = "INSERT INTO public.cliente (id, deleted, id_empresa, id_habil, cpf_cnpj, data_nascimento, email, endereco_bairro,\n" +
      "                            endereco_cep, endereco_complemento, endereco_logradouro, endereco_municipio,\n" +
      "                            endereco_numero, endereco_pais, endereco_uf, endereco_carne_bairro, endereco_carne_cep,\n" +
      "                            endereco_carne_complemento, endereco_carne_logradouro, endereco_carne_municipio,\n" +
      "                            endereco_carne_numero, endereco_carne_pais, endereco_carne_uf, estado_civil, naturalidade,\n" +
      "                            nome, nome_mae, nome_pai, pessoa_juridica, razao_social, rg_ie, sexo, id_profissao)\n" +
      "VALUES (:id, :deleted, :id_empresa, :id_habil, :cpf_cnpj, :data_nascimento, :email, :endereco_bairro, :endereco_cep,\n" +
      "        :endereco_complemento, :endereco_logradouro, :endereco_municipio, :endereco_numero, :endereco_pais,\n" +
      "        :endereco_uf, :endereco_carne_bairro, :endereco_carne_cep, :endereco_carne_complemento,\n" +
      "        :endereco_carne_logradouro, :endereco_carne_municipio, :endereco_carne_numero, :endereco_carne_pais,\n" +
      "        :endereco_carne_uf, :estado_civil, :naturalidade, :nome, :nome_mae, :nome_pai, :pessoa_juridica, :razao_social,\n" +
      "        :rg_ie, :sexo, :id_profissao)\n";
}
