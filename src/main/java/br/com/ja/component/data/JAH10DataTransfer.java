package br.com.ja.component.data;

import br.com.ja.component.data.transfer.ClienteDataTransfer;
import br.com.ja.util.Timer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.SQLException;

@Component
public class JAH10DataTransfer implements IDBConnectionProvider {

  @Autowired
  private ClienteDataTransfer cliente;


  @Override
  public Connection h10() throws SQLException {
    DriverManagerDataSource ds = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/tn12466720000126", "habilx", "koihw2017");
    ds.setDriverClassName("org.postgresql.Driver");
    return ds.getConnection();
  }

  @Override
  public Connection ja() throws SQLException {
    DriverManagerDataSource ds = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/ja_assessoria", "habilx", "koihw2017");
    ds.setDriverClassName("org.postgresql.Driver");
    return ds.getConnection();
  }

  public void doDownloadData() {
    Timer.<Void>process(() -> {
      cliente.download(this);
      return null;
    }, "Download de Clientes");
  }


}
