package br.com.ja.component.data.transfer;

import br.com.ja.component.data.IDBConnectionProvider;

public interface IDataTransfer {

  void download(IDBConnectionProvider provider);

  void upload(IDBConnectionProvider provider);
}
