package br.com.ja.component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

@Component
public class CustomDateSerializer extends StdSerializer<Date> {

  SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

  protected CustomDateSerializer(Class<Date> t) {
    super(t);
  }

  public CustomDateSerializer() {
    this(null);
  }

  @Override
  public void serialize(Date value, JsonGenerator gen, SerializerProvider provider) throws IOException {
    fmt.setTimeZone(TimeZone.getDefault()); // red#2839
    gen.writeString(fmt.format(value));
  }
}
