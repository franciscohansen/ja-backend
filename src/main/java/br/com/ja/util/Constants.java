package br.com.ja.util;

public class Constants {

  public static final String UNACCENT_FUNCTION = "unaccent";
  public static final String DADOS_CLIENTE = "dadosCliente";
  public static final String SITUACAO = "situacao";
  public static final String NOT_IMPLEMENTED = "Not implemented";


  private Constants() {

  }

}
