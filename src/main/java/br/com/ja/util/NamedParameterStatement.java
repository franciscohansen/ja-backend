package br.com.ja.util;

import lombok.Getter;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class NamedParameterStatement {


  private final String sql;

  Map<Integer, ParamNameValue> params = new HashMap<>();


  public NamedParameterStatement(String sql, Connection connection) throws SQLException {
    this.sql = sql;

  }

  public void setObject(String pName, Object value, int type) {
    throw new UnsupportedOperationException("Not Implemented");
  }

  @Getter
  private class ParamNameValue {
    private final String name;
    private final Object value;
    private final int type;

    private ParamNameValue(String name, Object value, int type) {
      this.name = name;
      this.value = value;
      this.type = type;
    }
  }


}
