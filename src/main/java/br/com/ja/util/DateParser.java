package br.com.ja.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateParser {

  private final String dateValue;
  private String pattern;

  public static final String YYYYMMDD = "yyyy-MM-dd";
  public static final String YYYYMMDDHHMMSSSSS = "yyyy-MM-dd HH:mm:ss.SSS";

  public DateParser(String dateValue, boolean somenteData) {
    this.dateValue = dateValue;
    this.pattern = YYYYMMDD;
  }

  public DateParser(String dateValue) {
    this.dateValue = dateValue;
    this.pattern = YYYYMMDDHHMMSSSSS;
  }

  public static boolean isParseableDate(String dateValue) {
    try {
      parseDate(dateValue);
      return true;
    } catch (ParseException e) {
      return false;
    }
  }

  public static Date parseDate(String dateValue) throws ParseException {
    return new DateParser(dateValue).parse();
  }

  public static Date parseDate(String dateValue, boolean somenteData) throws ParseException {
    return new DateParser(dateValue, somenteData).parse();
  }

  private void changePattern() {
    switch (pattern) {
      case YYYYMMDDHHMMSSSSS: {
        pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        break;
      }
      case "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'": {
        pattern = "yyyy-MM-dd'T'HH:mm:ssZ";
        break;
      }
      case "yyyy-MM-dd'T'HH:mm:ssZ": {
        pattern = "dd/MM/yyyy HH:mm:ssZ";
        break;
      }
      case "dd/MM/yyyy HH:mm:ssZ": {
        pattern = YYYYMMDD;
        break;
      }
      case YYYYMMDD: {
        pattern = "dd/MM/yyyy";
        break;
      }
      case "dd/MM/yyyy": {
        pattern = "HH:mm:ss.SSS";
        break;
      }
      case "HH:mm:ss.SSS": {
        pattern = "HH:mm:ssZ";
        break;
      }
      case "HH:mm:ssZ": {
        pattern = "HH:mm:ss";
        break;
      }
      default:
        pattern = null;
    }
  }

  private Date parse() throws ParseException {
    if (pattern == null) {
      throw new ParseException("Unparseable date:" + this.dateValue, 0);
    }
    try {
      SimpleDateFormat sdf = new SimpleDateFormat(pattern);
      return sdf.parse(dateValue);
    } catch (ParseException e) {
      changePattern();
      return parse();
    }
  }
}