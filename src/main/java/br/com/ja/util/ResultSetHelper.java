package br.com.ja.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class ResultSetHelper {
  private static final Logger LOGGER = LoggerFactory.getLogger(ResultSetHelper.class);
  public ResultSetHelper(ResultSet rs) {
    this.rs = rs;
  }

  public static <T> T getOrDefault(ResultSet rs, String field, T defaultValue, Class<T> clz) {
    try {
      T t = rs.getObject(field, clz);
      return t != null ? t : defaultValue;
    } catch (SQLException error) {
      return defaultValue;
    }
  }

  private final ResultSet rs;

  public Long getLong(String field, Long defaultValue) {
    return getOrDefault(rs, field, defaultValue, Long.class);
  }

  public Boolean getBoolean(String field, Boolean defaultValue) {
    return getOrDefault(rs, field, defaultValue, Boolean.class);
  }

  public Boolean getBoolean(String field) {
    return getBoolean(field, Boolean.FALSE);
  }

  public String getString(String field, String defaultValue) {
    return getOrDefault(rs, field, defaultValue, String.class);
  }

  public String getString(String field) {
    return getOrDefault(rs, field, "", String.class);
  }

  public Date getDate(String field, Date defaultValue) {
    return getOrDefault(rs, field, defaultValue, Date.class);
  }

  public Date getDate(String field) {
    return getOrDefault(rs, field, null, Date.class);
  }

}
