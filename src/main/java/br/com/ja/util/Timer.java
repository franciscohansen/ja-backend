package br.com.ja.util;

import org.springframework.util.StopWatch;

import java.util.function.Supplier;
import java.util.logging.Logger;

public class Timer {

  private static final Logger LOG = Logger.getLogger(Timer.class.getSimpleName());

  private Timer() {

  }

  public static <T> T process(Supplier<T> supplier, String task) {
    StopWatch watch = new StopWatch();
    watch.start(task);
    T response = supplier.get();
    watch.stop();
    LOG.info(() -> String.format("%s: %ss", task, watch.getTotalTimeSeconds()));
    return response;
  }
}
