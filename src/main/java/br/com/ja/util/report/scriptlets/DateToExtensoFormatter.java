package br.com.ja.util.report.scriptlets;

import net.sf.jasperreports.engine.JRDefaultScriptlet;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class DateToExtensoFormatter extends JRDefaultScriptlet {

  public String toExtenso(Date value) {
    LocalDate ld = value.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    SimpleDateFormat day = new SimpleDateFormat("dd");
    SimpleDateFormat year = new SimpleDateFormat("yyyy");

    return day.format(value) + " de " + monthDescription(ld.getMonthValue()) + " de " + year.format(value);
  }

  private String monthDescription(int month) {
    switch (month) {
      case 1:
        return "Janeiro";
      case 2:
        return "Fevereiro";
      case 3:
        return "Março";
      case 4:
        return "Abril";
      case 5:
        return "Maio";
      case 6:
        return "Junho";
      case 7:
        return "Julho";
      case 8:
        return "Agosto";
      case 9:
        return "Setembro";
      case 10:
        return "Outubro";
      case 11:
        return "Novembro";
      case 12:
        return "Dezembro";
      default:
        return "";
    }
  }

}
