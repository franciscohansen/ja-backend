package br.com.ja.util.report;

import br.com.ja.dto.ReportParamDTO;
import br.com.ja.model.Configuracao;
import br.com.ja.persistence.repositories.ConfiguracaoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ReportPrinter {

  private static final String BASE_URL = "http://%s:%d/report/";
  public static final String TOKEN = "X-Auth-Token";

  private final ObjectMapper om;
  private final ConfiguracaoRepository configuracaoRepository;
  private final RestTemplate rt;


  @Autowired
  public ReportPrinter(ObjectMapper om, ConfiguracaoRepository configuracaoRepository) {
    this.om = om;
    this.configuracaoRepository = configuracaoRepository;
    this.rt = new RestTemplate();
  }

  private String baseUrl() {
    List<Configuracao> list = this.configuracaoRepository.findAll();
    if (!list.isEmpty()) {
      Configuracao cfg = list.get(0);
      return String.format(BASE_URL, cfg.getHostH10(), cfg.getPortH10());
    } else {
      return String.format(BASE_URL, "localhost", 4567);
    }
  }

  private ReportParamDTO buildDto(Long id, Long reportId) {
    Map<String, Object> params = new HashMap<>();
    params.put("id", id);
    params.put("idinicial", id);
    params.put("idfinal", id);
    params.put("empresa", 1L);
    params.put("empresain", Collections.singletonList(1L));
    return ReportParamDTO.builder()
        .reportId(reportId)
        .parametros(params)
        .formato("PDF")
        .build();
  }

  private String sendRequest(ReportParamDTO dto, String token) throws IOException, URISyntaxException {
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    om.writeValue(out, dto);
    String reportParams = new String(out.toByteArray(), StandardCharsets.UTF_8);
    String url =this.baseUrl() + "generate";

    HttpHeaders headers = new HttpHeaders();
    headers.set(TOKEN, token);

    MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    params.add("requestParam", reportParams);

    HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(params, headers);

    ResponseEntity<String> response = this.rt.postForEntity(new URI(url), request, String.class);
    if (response.getStatusCode().equals(HttpStatus.OK)) {
      return response.getBody();
    } else {
      return null;
    }
  }

  public String printReport(Long id, Long reportId, String token) throws IOException, URISyntaxException {
    ReportParamDTO dto = this.buildDto(id, reportId);
    return this.sendRequest(dto, token);
  }


}
