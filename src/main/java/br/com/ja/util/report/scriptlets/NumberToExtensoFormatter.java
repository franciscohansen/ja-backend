package br.com.ja.util.report.scriptlets;

import br.com.caelum.stella.inwords.FormatoDeReal;
import br.com.caelum.stella.inwords.NumericToWordsConverter;
import net.sf.jasperreports.engine.JRDefaultScriptlet;

public class NumberToExtensoFormatter extends JRDefaultScriptlet {

  public String toExtenso( double value ) {
    return new NumericToWordsConverter( new FormatoDeReal() ).toWords( value );
  }
}
