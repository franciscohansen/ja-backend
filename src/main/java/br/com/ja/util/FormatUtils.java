package br.com.ja.util;

import javax.swing.text.MaskFormatter;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FormatUtils {

  private FormatUtils(){

  }

  public static String getFormattedDoc(String doc) {
    String sDoc = removeNonAlphaNumericCharacters(doc);
    if (sDoc.length() > 11) {
      return getFormattedCNPJ(sDoc);
    } else {
      return getFormattedCPF(sDoc);
    }
  }

  public static String getFormattedCNPJ(String cnpj) {
    try {
      MaskFormatter mf = new MaskFormatter("AA.AAA.AAA/AAAA-AA");
      mf.setValueContainsLiteralCharacters(false);
      return mf.valueToString(cnpj);
    } catch (ParseException ex) {
      Logger.getLogger(FormatUtils.class.getName()).log(Level.SEVERE, null, ex);
      return null;
    }
  }

  /**
   * Retorna o cpf no formato XXX.XXX.XXX-XX
   *
   * @param cpf
   * @return
   */
  public static String getFormattedCPF(String cpf) {
    try {
      MaskFormatter mf = new MaskFormatter("AAA.AAA.AAA-AA");
      mf.setValueContainsLiteralCharacters(false);
      return mf.valueToString(cpf);
    } catch (ParseException ex) {
      Logger.getLogger(FormatUtils.class.getName()).log(Level.SEVERE, null, ex);
      return null;
    }
  }

  public static String removeNonAlphaNumericCharacters(String s) {
    if (s == null) {
      return "";
    }
    return s.replaceAll("[^A-Za-z0-9]", "");
  }
}
