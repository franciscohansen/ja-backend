package br.com.ja.config;

import br.com.ja.component.CustomDateDeserializer;
import br.com.ja.component.CustomDateSerializer;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Date;
import java.util.List;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

  @Autowired
  private ObjectMapper mapper;
  @Autowired
  private CustomDateDeserializer dateDeserializer;
  @Autowired
  private CustomDateSerializer dateSerializer;


  private MappingJackson2HttpMessageConverter customJackson2HttpMessageConverter() {
    SimpleModule module = new SimpleModule("ja-assessoria", Version.unknownVersion());

    module.addDeserializer(Date.class, dateDeserializer);
    module.addSerializer(Date.class, dateSerializer);

    mapper.registerModule(module);

    mapper.registerModule(new Jdk8Module());
    mapper.registerModule(module());
    mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

    MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
    converter.setObjectMapper(mapper);
    return converter;
  }

  @Bean
  public Hibernate5Module module() {
    Hibernate5Module hibernate5Module = new Hibernate5Module();
    hibernate5Module.enable(Hibernate5Module.Feature.FORCE_LAZY_LOADING);
    return hibernate5Module;
  }

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    //This is empty
  }

  @Override
  public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
    converters.add(new StringHttpMessageConverter());
    converters.add(new FormHttpMessageConverter());
    converters.add(customJackson2HttpMessageConverter());
  }

}
