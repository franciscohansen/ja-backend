package br.com.ja.controller;

import br.com.ja.model.AbstractModel;
import br.com.ja.persistence.repositories.IRepository;
import br.com.ja.persistence.service.AbstractService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

public abstract class AbstractController<T extends AbstractModel, U extends IRepository<T>, V extends AbstractService<T, U>> {

  protected abstract U getRepository();

  protected abstract V getService();

  protected abstract T newInstance();

  protected abstract ObjectMapper om();

  protected abstract Class<T> clazz();


  @GetMapping("/novo")
  public T novo() {
    return newInstance();
  }

  @GetMapping("/{id}")
  public ResponseEntity<T> findById(@PathVariable Long id) {
    Optional<T> optional = getRepository().findById(id);
    return optional.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.noContent().build());
  }

  @GetMapping("/id-habil/{id}")
  public ResponseEntity<T> findByIdHabil(@PathVariable("id") Long idHabil) {
    Optional<T> optional = getRepository().findByIdHabil(idHabil);
    return optional.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.noContent().build());
  }

  protected abstract Specification<T> findAllSpecification(HttpServletRequest request, String search);

  @GetMapping("/{start}/{length}")
  public List<T> findAll(HttpServletRequest request, @RequestParam("search") String search, @PathVariable int start, @PathVariable int length) {
    if (length > 0) {
      return lazyLoad(getRepository()
          .findAll(Specification.where(findAllSpecification(request, search)), PageRequest.of(start, length))
          .toList());
    } else {
      return lazyLoad(getRepository()
          .findAll(Specification.where(findAllSpecification(request, search))));
    }
  }

  protected List<T> lazyLoad(List<T> list) {
    return list;
  }

  protected T lazyLoad(T obj) {
    return obj;
  }

  @GetMapping("/count")
  public Long count(HttpServletRequest request, @RequestParam("search") String search) {
    return getRepository().count(Specification.where(findAllSpecification(
        request,
        search
    )));
  }

  @PostMapping(value = "/save", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
  public ResponseEntity<T> save(@RequestParam("obj") String str) throws JsonProcessingException {
    T obj = this.om().readValue(str, clazz());
    boolean bInsertion = obj.getId() == null || obj.getId() == 0;
    obj = getService().save(obj);
    return new ResponseEntity<>(obj, bInsertion ? HttpStatus.CREATED : HttpStatus.OK);
  }

  @DeleteMapping("/{id}")
  public Boolean delete(@PathVariable("id") Long id) {
    Optional<T> optional = this.getRepository().findById(id);
    if (optional.isPresent()) {
      T obj = optional.get();
      obj.setDeleted(true);
      this.getService().save(obj);
    }
    return true;
  }

  @PostMapping("/{id}/print/{report}")
  public String pring(@PathVariable("id") Long id, @PathVariable("report") Long reportId) throws IOException, URISyntaxException {
    return this.getService().imprimir(id, reportId);
  }

}
