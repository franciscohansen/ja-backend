package br.com.ja.controller;

import br.com.ja.model.Endereco;
import br.com.ja.model.Forum;
import br.com.ja.persistence.repositories.ForumRepository;
import br.com.ja.persistence.service.ForumService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@RestController
@RequestMapping("/forum")
public class ForumController extends AbstractController<Forum, ForumRepository, ForumService> {
  @Autowired
  private ForumRepository repository;
  @Autowired
  private ForumService service;
  @Autowired
  private ObjectMapper om;

  @Override
  protected ForumRepository getRepository() {
    return repository;
  }

  @Override
  protected ForumService getService() {
    return service;
  }

  @Override
  protected Forum newInstance() {
    return Forum.builder().endereco(Endereco.builder().build()).build();
  }

  @Override
  protected ObjectMapper om() {
    return om;
  }

  @Override
  protected Class<Forum> clazz() {
    return Forum.class;
  }

  @Override
  protected Specification<Forum> findAllSpecification(HttpServletRequest request, String search) {
    return (root, query, builder) -> {
      String sLike = "%" + Optional.ofNullable(search).orElse("") + "%";
      return builder.and(
          builder.isFalse(builder.coalesce(root.get("deleted"), false)),
          builder.like(builder.function("unaccent", String.class, builder.lower(root.get("nome"))), sLike)
      );
    };
  }
}
