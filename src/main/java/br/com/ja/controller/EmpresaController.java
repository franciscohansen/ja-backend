package br.com.ja.controller;

import br.com.ja.model.Empresa;
import br.com.ja.persistence.repositories.EmpresaRepository;
import br.com.ja.persistence.service.EmpresaService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/empresa")
public class EmpresaController extends AbstractController<Empresa, EmpresaRepository, EmpresaService> {

  @Autowired
  private EmpresaRepository repository;
  @Autowired
  private EmpresaService service;
  @Autowired
  private ObjectMapper om;

  @Override
  protected EmpresaRepository getRepository() {
    return repository;
  }

  @Override
  protected EmpresaService getService() {
    return service;
  }

  @Override
  protected Empresa newInstance() {
    return Empresa.builder().build();
  }

  @Override
  protected ObjectMapper om() {
    return om;
  }

  @Override
  protected Class<Empresa> clazz() {
    return Empresa.class;
  }

  @Override
  protected Specification<Empresa> findAllSpecification(HttpServletRequest request, String search) {
    return (root, query, builder) -> builder.isFalse(builder.coalesce(root.get("deleted"), false));
  }
}
