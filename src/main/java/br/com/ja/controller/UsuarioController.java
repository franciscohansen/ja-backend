package br.com.ja.controller;

import br.com.ja.model.Usuario;
import br.com.ja.persistence.repositories.UsuarioRepository;
import br.com.ja.persistence.service.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/usuario")
public class UsuarioController extends AbstractController<Usuario, UsuarioRepository, UsuarioService> {

  @Autowired
  private UsuarioRepository repository;
  @Autowired
  private UsuarioService service;
  @Autowired
  private ObjectMapper om;

  @Override
  protected UsuarioRepository getRepository() {
    return repository;
  }

  @Override
  protected UsuarioService getService() {
    return service;
  }

  @Override
  protected Usuario newInstance() {
    return Usuario.builder().build();
  }

  @Override
  protected ObjectMapper om() {
    return om;
  }

  @Override
  protected Class<Usuario> clazz() {
    return Usuario.class;
  }

  @Override
  protected Specification<Usuario> findAllSpecification(HttpServletRequest request, String search) {
    return (root, query, builder) -> builder.isFalse(builder.coalesce(root.get("deleted"), false));
  }


}
