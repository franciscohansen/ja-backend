package br.com.ja.controller;

import br.com.ja.model.contatos.ContatoSimulacao;
import br.com.ja.model.juridico.EJuridicoSituacao;
import br.com.ja.model.juridico.Juridico;
import br.com.ja.model.juridico.JuridicoVisualizacao;
import br.com.ja.model.juridico.situacao.JuridicoSituacao;
import br.com.ja.persistence.repositories.ContatoSimulacaoRepository;
import br.com.ja.persistence.repositories.JuridicoRepository;
import br.com.ja.persistence.repositories.JuridicoVisualizacaoRepository;
import br.com.ja.persistence.repositories.UsuarioRepository;
import br.com.ja.persistence.service.ContatoSimulacaoService;
import br.com.ja.persistence.service.JuridicoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.Predicate;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/juridico")
public class JuridicoController extends AbstractController<Juridico, JuridicoRepository, JuridicoService> {

  @Autowired
  private JuridicoRepository repository;
  @Autowired
  private JuridicoService service;
  @Autowired
  private ObjectMapper om;

  @Autowired
  private ContatoSimulacaoRepository csRepository;
  @Autowired
  private ContatoSimulacaoService csService;
  @Autowired
  private UsuarioRepository usuarioRepository;
  @Autowired
  private JuridicoVisualizacaoRepository jvRepository;

  @Override
  protected JuridicoRepository getRepository() {
    return repository;
  }

  @Override
  protected JuridicoService getService() {
    return service;
  }

  @Override
  protected Juridico newInstance() {
    List<JuridicoSituacao> situacoes = new ArrayList<>();
    for (EJuridicoSituacao sit : EJuridicoSituacao.values()) {
      situacoes.add(
          JuridicoSituacao.builder()
              .situacao(sit)
              .build()
      );
    }

    return Juridico.builder()
        .links(new HashSet<>())
        .situacoes(situacoes)
        .build();
  }

  @Override
  protected ObjectMapper om() {
    return om;
  }

  @Override
  protected Class<Juridico> clazz() {
    return Juridico.class;
  }

  @Override
  protected Specification<Juridico> findAllSpecification(HttpServletRequest request, String search) {
    return (root, query, builder) -> {
      Predicate where = builder.isFalse(builder.coalesce(root.get("deleted"), false));

      query.orderBy(builder.desc(root.get("id")));
      return where;
    };
  }

  @GetMapping("/locate/{start}/{length}")
  public List<ContatoSimulacao> findAllJuridico(HttpServletRequest request, @RequestParam("search") String search, @PathVariable int start, @PathVariable int length) {
    List<ContatoSimulacao> list = null;
    if (length > 0) {
      list = this.csRepository.findAll(Specification.where(csService.juridicoFindAll(request, search)), PageRequest.of(start, length)).toList();
    } else {
      list = this.csRepository.findAll(Specification.where(csService.juridicoFindAll(request, search)));
    }

    return list;
  }

  @GetMapping("/locate/count")
  public Long countJuridico(HttpServletRequest request, @RequestParam("search") String search) {
    return csRepository.count(Specification.where(csService.juridicoFindAll(request, search)));
  }

  @GetMapping("/situacao-ativa/{id}")
  @Transactional
  public JuridicoSituacao situacaoAtiva(@PathVariable Long id) {
    if (id > 0) {
      Juridico juridico = repository.getById(id);
      return juridico.getSituacoes().stream().filter(JuridicoSituacao::getAtivo).findAny().orElse(JuridicoSituacao.builder().situacao(EJuridicoSituacao.SEM_BUSCA).build());
    } else {
      return JuridicoSituacao.builder().situacao(EJuridicoSituacao.SEM_BUSCA).build();
    }
  }

  @PostMapping("/muda-situacao/{id}")
  public JuridicoSituacao mudaSituacao(@PathVariable Long id, @RequestHeader("situacao") String situacao, @RequestHeader("ativo") Boolean ativo) {
    Juridico juridico = repository.getById(id);
    List<JuridicoSituacao> situacoes = juridico.getSituacoes();
    for (JuridicoSituacao js : situacoes) {
      js.setAtivo(false);
      if (js.getSituacao().equals(EJuridicoSituacao.valueOf(situacao))) {
        js.setAtivo(ativo);
        js.setData(Calendar.getInstance().getTime());
      }
    }
    if (juridico.getSituacoes().stream().noneMatch(JuridicoSituacao::getAtivo)) {
      juridico.getSituacoes().stream().filter(j -> j.getSituacao().equals(EJuridicoSituacao.SEM_BUSCA))
          .findFirst()
          .ifPresent(j -> j.setAtivo(true));
    }
    repository.save(juridico);
    return situacaoAtiva(id);
  }

  @GetMapping("/visualizacoes/{id}")
  @Transactional
  public JuridicoVisualizacao visualizacoes(@PathVariable Long id,
                                            @RequestHeader("id-usuario") Long idUsuario) {
      Optional<Juridico> juridicoOpt = repository.findById(id);
      if( juridicoOpt.isPresent() ) {
        Juridico juridico = juridicoOpt.get();
        Optional<JuridicoVisualizacao> visualizacoes = juridico.getVisualizacoes()
            .stream().filter(v -> {
              Date vDate = v.getData();
              LocalDate now = LocalDate.now();
              LocalDate then = vDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
              return v.getUsuario().getId().equals(idUsuario) && (now.isEqual(then));
            }).collect(Collectors.toList()).stream().findFirst();
        JuridicoVisualizacao jv = JuridicoVisualizacao.builder()
            .data(Calendar.getInstance().getTime())
            .nroVisualizacoes(0)
            .juridico(juridico)
            .usuario(usuarioRepository.getById(idUsuario))
            .build();
        if (!visualizacoes.isPresent()) {
          jv = this.jvRepository.save(jv);
        } else {
          jv = visualizacoes.get();
        }
        return jv;
      }else{
        return JuridicoVisualizacao.builder().nroVisualizacoes(0).build();
      }
  }

  @PostMapping("/marca-lido/{id}")
  public JuridicoVisualizacao marcaLido(@PathVariable("id") Long id, @RequestHeader("id-usuario") Long idUsuario) {
    Juridico juridico = repository.getById(id);
    Optional<JuridicoVisualizacao> visualizacoes = juridico.getVisualizacoes()
        .stream()
        .filter(v -> {
          Date vDate = v.getData();
          LocalDate now = LocalDate.now();
          LocalDate then = vDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
          return v.getUsuario().getId().equals(idUsuario) && (now.isEqual(then));
        })
        .collect(Collectors.toList())
        .stream()
        .findFirst();
    JuridicoVisualizacao jv = JuridicoVisualizacao.builder()
        .data(Calendar.getInstance().getTime())
        .nroVisualizacoes(1)
        .juridico(juridico)
        .usuario(usuarioRepository.getById(idUsuario))
        .build();
    if (!visualizacoes.isPresent()) {
      jv = this.jvRepository.save(jv);
    } else {
      jv = visualizacoes.get();
      jv.setNroVisualizacoes(jv.getNroVisualizacoes() + 1);
      jv = jvRepository.save(jv);
    }
    return jv;
  }

  @PostMapping("/corrige-situacoes")
  public Boolean criaSituacoesFaltantes() {
    this.service.criaSituacoesFaltantes();
    return true;
  }

}
