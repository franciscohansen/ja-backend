package br.com.ja.controller;

import br.com.ja.model.FollowUp;
import br.com.ja.persistence.repositories.FollowUpRepository;
import br.com.ja.persistence.service.FollowUpService;
import br.com.ja.projections.FollowUpMotivoProjection;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.criteria.Predicate;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@RequestMapping("/follow-up")
public class FollowUpController extends AbstractController<FollowUp, FollowUpRepository, FollowUpService> {

  private static final Logger LOG = Logger.getLogger(FollowUpController.class.getSimpleName());
  @Autowired
  private FollowUpRepository repository;
  @Autowired
  private FollowUpService service;
  @Autowired
  private ObjectMapper om;

  @Override
  protected FollowUpRepository getRepository() {
    return repository;
  }

  @Override
  protected FollowUpService getService() {
    return service;
  }

  @Override
  protected FollowUp newInstance() {
    return FollowUp.builder().build();
  }

  @Override
  protected ObjectMapper om() {
    return om;
  }

  @Override
  protected Class<FollowUp> clazz() {
    return FollowUp.class;
  }

  @Override
  protected Specification<FollowUp> findAllSpecification(HttpServletRequest request, String search) {
    return (root, query, builder) -> {
      Predicate where = builder.isFalse(builder.coalesce(root.get("deleted"), false));
      String sID = request.getHeader("id-cliente");
      if (!Optional.ofNullable(sID).orElse("").isEmpty()) {
        where = builder.and(
            where,
            builder.equal(root.get("cliente").get("id"), Long.valueOf(sID))
        );
      }
      String sDI = request.getHeader("data-inicial");
      String sDF = request.getHeader("data-final");
      if (!Optional.ofNullable(sDI).orElse("").isEmpty() &&
          !Optional.ofNullable(sDF).orElse("").isEmpty()) {
        try {
          SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
          Date di = sdf.parse(sDI);
          Date df = sdf.parse(sDF);
          where = builder.and(
              where,
              builder.between(root.get("dataHora"), di, df)
          );
        } catch (ParseException e) {
          LOG.log(Level.SEVERE, null, e);
        }
      }
      return where;
    };
  }

  @GetMapping("/juridico/{id}")
  public List<FollowUp> findAllByIdJuridico(@PathVariable Long id) {
    return getRepository().findAllByJuridicoIdOrderByIdDesc(id);
  }

  @GetMapping("/motivos")
  public List<FollowUpMotivoProjection> findDistinctMotivos() {
    return getRepository().findAllMotivos();
  }

}
