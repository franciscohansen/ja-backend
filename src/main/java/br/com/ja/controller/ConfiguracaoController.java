package br.com.ja.controller;

import br.com.ja.model.Configuracao;
import br.com.ja.persistence.repositories.ConfiguracaoRepository;
import br.com.ja.persistence.service.ConfiguracaoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/configuracao")
public class ConfiguracaoController extends AbstractController<Configuracao, ConfiguracaoRepository, ConfiguracaoService> {

  @Autowired
  private ConfiguracaoRepository repository;
  @Autowired
  private ConfiguracaoService service;
  @Autowired
  private ObjectMapper om;

  @Override
  protected ConfiguracaoRepository getRepository() {
    return repository;
  }

  @Override
  protected ConfiguracaoService getService() {
    return service;
  }

  @Override
  protected Configuracao newInstance() {
    return Configuracao.builder().build();
  }

  @Override
  protected ObjectMapper om() {
    return om;
  }

  @Override
  protected Class<Configuracao> clazz() {
    return Configuracao.class;
  }

  @Override
  protected Specification<Configuracao> findAllSpecification(HttpServletRequest request, String search) {
    return (root, query, builder) -> builder.isFalse(builder.coalesce(root.get("deleted"), false));
  }

  @GetMapping("/get")
  public Configuracao get() {
    Configuracao cfg = Configuracao.builder().build();
    List<Configuracao> list = this.repository.findAll();
    if (!list.isEmpty()) {
      cfg = list.get(0);
    }
    return this.service.save(cfg);
  }
}
