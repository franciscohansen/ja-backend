package br.com.ja.controller;

import br.com.ja.model.juridico.situacao.JuridicoSituacao;
import br.com.ja.persistence.repositories.JuridicoSituacaoRepository;
import br.com.ja.persistence.service.JuridicoSituacaoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/juridico-situacao")
public class JuridicoSituacaoController extends AbstractController<JuridicoSituacao, JuridicoSituacaoRepository, JuridicoSituacaoService> {

  @Autowired
  private JuridicoSituacaoRepository repository;
  @Autowired
  private JuridicoSituacaoService service;
  @Autowired
  private ObjectMapper om;

  @Override
  protected JuridicoSituacaoRepository getRepository() {
    return repository;
  }

  @Override
  protected JuridicoSituacaoService getService() {
    return service;
  }

  @Override
  protected JuridicoSituacao newInstance() {
    return JuridicoSituacao.builder().build();
  }

  @Override
  protected ObjectMapper om() {
    return om;
  }

  @Override
  protected Class<JuridicoSituacao> clazz() {
    return JuridicoSituacao.class;
  }

  @Override
  protected Specification<JuridicoSituacao> findAllSpecification(HttpServletRequest request, String search) {
    return (root, query, builder) -> builder.isFalse(builder.coalesce(root.get("deleted"), false));
  }

  @GetMapping("/id-juridico/{id}")
  public List<JuridicoSituacao> findByIdJuridico(@PathVariable Long id) {
    return this.repository.findAllByJuridicoId(id);
  }
}
