package br.com.ja.controller;

import br.com.ja.model.Report;
import br.com.ja.persistence.repositories.ReportRepository;
import br.com.ja.persistence.service.ReportService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.criteria.Predicate;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/report")
public class ReportController extends AbstractController<Report, ReportRepository, ReportService> {

  private ReportRepository repository;
  private ReportService service;
  private ObjectMapper om;

  @Autowired
  public ReportController(ReportRepository repository, ReportService service, ObjectMapper om) {
    this.repository = repository;
    this.service = service;
    this.om = om;
  }

  @Override
  protected ReportRepository getRepository() {
    return repository;
  }

  @Override
  protected ReportService getService() {
    return service;
  }

  @Override
  protected Report newInstance() {
    return Report.builder().build();
  }

  @Override
  protected ObjectMapper om() {
    return om;
  }

  @Override
  protected Class<Report> clazz() {
    return Report.class;
  }

  @Override
  protected Specification<Report> findAllSpecification(HttpServletRequest request, String search) {
    return (root, query, builder) -> {
      Predicate where = builder.isFalse(builder.coalesce(root.get("deleted"), false));
      query.orderBy(builder.desc(root.get("id")));
      return where;
    };
  }
}
