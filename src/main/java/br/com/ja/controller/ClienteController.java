package br.com.ja.controller;

import br.com.ja.dto.ClienteUFDTO;
import br.com.ja.exceptions.DocumentConflictException;
import br.com.ja.model.cliente.Cliente;
import br.com.ja.model.cliente.ClienteTelefones;
import br.com.ja.persistence.repositories.ClienteRepository;
import br.com.ja.persistence.service.ClienteService;
import br.com.ja.util.FormatUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.Predicate;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

import static br.com.ja.util.Constants.UNACCENT_FUNCTION;

@RestController
@RequestMapping("/cliente")
public class ClienteController extends AbstractController<Cliente, ClienteRepository, ClienteService> {
  @Autowired
  private ClienteRepository repository;
  @Autowired
  private ClienteService service;
  @Autowired
  private ObjectMapper om;

  @Override
  protected ClienteRepository getRepository() {
    return repository;
  }

  @Override
  protected ClienteService getService() {
    return service;
  }

  @Override
  protected Cliente newInstance() {
    return Cliente.builder().build();
  }

  @Override
  protected ObjectMapper om() {
    return om;
  }

  @Override
  protected Class<Cliente> clazz() {
    return Cliente.class;
  }

  @Override
  protected Specification<Cliente> findAllSpecification(HttpServletRequest request, String search) {
    return (root, query, builder) -> {
      String sLike = "%" + search.toLowerCase() + "%";
      Predicate where = builder.isFalse(
          builder.coalesce(root.get("deleted"), false)
      );
      where = builder.and(
          where,
          builder.or(
              builder.like(builder.function(UNACCENT_FUNCTION, String.class, builder.lower(root.get("nome"))), sLike),
              builder.like(builder.function(UNACCENT_FUNCTION, String.class, builder.lower(root.get("razaoSocial"))), sLike),
              builder.like(builder.function(UNACCENT_FUNCTION, String.class, builder.lower(root.get("cpfCnpj"))), sLike),
              builder.like(builder.function(UNACCENT_FUNCTION, String.class, builder.lower(root.get("rgIe"))), sLike)
          )
      );
      return where;
    };
  }

  @GetMapping("/ufs")
  public ClienteUFDTO ufs() {
    return ClienteUFDTO.builder().ufs(this.service.ufs()).build();
  }

  @GetMapping("/{id}/telefones")
  public List<ClienteTelefones> getTelefones(@PathVariable("id") long id) {
    return this.getRepository().getById(id).getTelefones();
  }

  @PostMapping("/documento/validate")
  public Boolean validateCpfCnpj(@RequestParam("documento") String documento) throws DocumentConflictException {
    return this.service.validateDocument(documento) && this.service.validateDocumentDoestNotExist(documento);
  }

  @GetMapping("/documento/locate/{document}")
  public Cliente locateCpfCnpj(@PathVariable(name = "document") String document) {
    String doc = FormatUtils.getFormattedDoc(FormatUtils.removeNonAlphaNumericCharacters(document));
    return this.repository.findFirstByCpfCnpj(doc);
  }

}
