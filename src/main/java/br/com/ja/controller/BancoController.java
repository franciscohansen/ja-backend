package br.com.ja.controller;

import br.com.ja.model.Banco;
import br.com.ja.persistence.repositories.BancoRepository;
import br.com.ja.persistence.service.BancoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.criteria.Predicate;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/bancos")
public class BancoController extends AbstractController<Banco, BancoRepository, BancoService> {

  @Autowired
  private BancoRepository repository;
  @Autowired
  private BancoService service;
  @Autowired
  private ObjectMapper om;

  @Override
  protected BancoRepository getRepository() {
    return this.repository;
  }

  @Override
  protected BancoService getService() {
    return this.service;
  }

  @Override
  protected Banco newInstance() {
    return new Banco();
  }

  @Override
  protected ObjectMapper om() {
    return this.om;
  }

  @Override
  protected Class<Banco> clazz() {
    return Banco.class;
  }

  @Override
  protected Specification<Banco> findAllSpecification(HttpServletRequest request, String search) {
    return ((root, query, builder) -> {
      String likeStr = "%" + search.toLowerCase() + "%";
      Predicate where = builder.isFalse(builder.coalesce(root.get("deleted"), false));
      where = builder.and(
          where,
          builder.like(builder.lower(builder.function("unaccent", String.class, root.get("descricao"))), likeStr)
      );
      query.orderBy(builder.desc(root.get("id")));
      return where;
    });
  }
}
