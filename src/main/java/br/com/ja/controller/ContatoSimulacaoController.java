package br.com.ja.controller;

import br.com.ja.model.Documento;
import br.com.ja.model.Endereco;
import br.com.ja.model.contatos.*;
import br.com.ja.model.contatos.dadosadicionais.Cartao;
import br.com.ja.model.contatos.dadosadicionais.DadosAdicionais;
import br.com.ja.model.contatos.dadosadicionais.Emprestimo;
import br.com.ja.model.contatos.dadosadicionais.Veiculo;
import br.com.ja.model.contatos.enums.EIndicacao;
import br.com.ja.model.contatos.enums.EStatusContato;
import br.com.ja.model.contatos.enums.ETipoContrato;
import br.com.ja.model.contatos.procurador.Procurador;
import br.com.ja.model.juridico.EJuridicoSituacao;
import br.com.ja.model.juridico.Juridico;
import br.com.ja.model.juridico.situacao.JuridicoSituacao;
import br.com.ja.persistence.repositories.ContatoSimulacaoRepository;
import br.com.ja.persistence.repositories.DocumentoRepository;
import br.com.ja.persistence.service.ContatoSimulacaoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.servlet.http.HttpServletRequest;
import java.net.URISyntaxException;
import java.util.*;

import static br.com.ja.util.Constants.*;

@RestController
@RequestMapping("/contatos")
public class ContatoSimulacaoController extends AbstractController<ContatoSimulacao, ContatoSimulacaoRepository, ContatoSimulacaoService> {
  @Autowired
  private ContatoSimulacaoRepository repository;
  @Autowired
  private ContatoSimulacaoService service;
  @Autowired
  private ObjectMapper om;
  @Autowired
  private DocumentoRepository documentoRepository;


  @Override
  protected ContatoSimulacaoRepository getRepository() {
    return repository;
  }

  @Override
  protected ContatoSimulacaoService getService() {
    return service;
  }

  @Override
  protected ContatoSimulacao newInstance() {
    List<Documento> documentos = this.documentoRepository.findAll();
    List<SimulacaoDocumentos> sDocs = new ArrayList<>();
    for (Documento d : documentos) {
      sDocs.add(
          SimulacaoDocumentos.builder()
              .descDocumento(d.getDescricao())
              .documento(d)
              .entregue(false)
              .build()
      );
    }

    return ContatoSimulacao.builder()
        .dataLcto(Calendar.getInstance().getTime())
        .tipoContrato(ETipoContrato.VEICULO)
        .status(EStatusContato.PENDENTE)
        .dadosIndicacao(
            Indicacao.builder()
                .indicacao(EIndicacao.CLIENTE)
                .build()
        )
        .dadosCliente(
            DadosCliente.builder()
                .endereco(Endereco.builder().build())
                .enderecoCarne(Endereco.builder().build())
                .telefones(new ArrayList<>())
                .build()
        )
        .adesao(
            Adesao.builder()
                .build()
        )
        .dadosAdicionais(
            DadosAdicionais.builder()
                .veiculo(Veiculo.builder().build())
                .cartao(Cartao.builder().build())
                .emprestimo(Emprestimo.builder().build())
                .build()
        )
        .procurador(
            Procurador.builder()
                .endereco(Endereco.builder().build())
                .build()
        )
        .simulacao(Simulacao.builder().build())
        .documentos(sDocs)
        .parcelas(new ArrayList<>())
        .build();
  }

  @Override
  protected ObjectMapper om() {
    return om;
  }

  @Override
  protected Class<ContatoSimulacao> clazz() {
    return ContatoSimulacao.class;
  }

  @Override
  protected Specification<ContatoSimulacao> findAllSpecification(HttpServletRequest request, String search) {
    return (root, query, builder) -> {
      String sLike = "%" + Optional.ofNullable(search).orElse("").toLowerCase() + "%";
      Predicate where = builder.isFalse(builder.coalesce(root.get("deleted"), false));
      String sHeader = request.getHeader(SITUACAO);
      sHeader = Optional.ofNullable(sHeader).orElse("TODOS");
      if (!sHeader.equals("TODOS")) {
        switch (sHeader) {
          case "ARQUIVADO": {
            where = builder.and(
                where,
                builder.isTrue(builder.coalesce(root.get("inativo"), false))
            );
            break;
          }
          case "CANCELADO": {
            Join<ContatoSimulacao, Juridico> juridicoJoin = root.join("juridico", JoinType.LEFT);
            Join<Juridico, JuridicoSituacao> situacaoJoin = juridicoJoin.join(SITUACAO, JoinType.LEFT);
            where = builder.and(
                where,
                builder.isTrue(builder.coalesce(root.get("cancelado"), false)),
                builder.equal(situacaoJoin.get(SITUACAO), EJuridicoSituacao.CONTRATO_CANCELADO),
                builder.isTrue(builder.coalesce(situacaoJoin.get("ativo"), false))
            );
            break;
          }
          default: {
            where = builder.and(
                where,
                builder.isFalse(builder.coalesce(root.get("inativo"), false)),
                builder.equal(root.get("status"), EStatusContato.valueOf(sHeader))
            );
          }
        }
      }

      where = builder.and(
          where,
          builder.or(
              builder.like(builder.function(UNACCENT_FUNCTION, String.class, builder.lower(root.get(DADOS_CLIENTE).get("nome"))), sLike),
              builder.like(builder.function(UNACCENT_FUNCTION, String.class, builder.lower(root.get(DADOS_CLIENTE).get("razaoSocial"))), sLike),
              builder.like(builder.function(UNACCENT_FUNCTION, String.class, builder.lower(root.get(DADOS_CLIENTE).get("cpfCnpj"))), sLike)
          )
      );

      query.orderBy(builder.desc(root.get("id")));
      return where;
    };
  }

  @PostMapping("/gera-venda")
  public ContatoSimulacao geraVenda(@RequestParam("id") Long id, @RequestParam(value = "somente-parcelas", defaultValue = "false") Boolean somenteParcelas) {
    return this.service.geraVenda(id, somenteParcelas);
  }

  @Override
  protected List<ContatoSimulacao> lazyLoad(List<ContatoSimulacao> list) {
    for (ContatoSimulacao cs : list) {
      lazyLoad(cs);
    }
    return list;
  }

  @Override
  protected ContatoSimulacao lazyLoad(ContatoSimulacao obj) {
    obj = super.lazyLoad(obj);
    List<SimulacaoParcelas> parcelas = obj.getParcelas();
    List<SimulacaoDocumentos> documentos = obj.getDocumentos();
    DadosCliente dados = obj.getDadosCliente();
    List<DadosClienteTelefone> fones = dados.getTelefones();
    return obj;
  }

  @GetMapping("/{id}/telefones")
  public List<DadosClienteTelefone> getTelefones(@PathVariable Long id) {
    ContatoSimulacao cs = getRepository().getById(id);
    DadosCliente dc = cs.getDadosCliente();
    if (dc != null) {
      return dc.getTelefones();
    } else {
      return Collections.emptyList();
    }
  }

  @GetMapping("/{id}/documentos")
  public List<SimulacaoDocumentos> getDocumentos(@PathVariable Long id) {
    ContatoSimulacao cs = getRepository().getById(id);
    return cs.getDocumentos();
  }

  @GetMapping("/{id}/parcelas")
  public List<SimulacaoParcelas> getParcelas(@PathVariable Long id) {
    ContatoSimulacao cs = getRepository().getById(id);
    return cs.getParcelas();
  }

  @PostMapping("/inativar/{id}")
  public void inativar(@PathVariable Long id) {
    getService().inativar(id);
  }

  @PostMapping("/{id}/cria-juridico")
  public boolean criaJuridico(@PathVariable Long id) {
    return this.service.criaJuridico(id);
  }

  @PostMapping("/{id}/cancelar")
  public boolean cancelar(@PathVariable Long id) throws URISyntaxException {
    return this.service.cancelar(id);
  }
}
