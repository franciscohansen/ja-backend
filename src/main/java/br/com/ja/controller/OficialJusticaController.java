package br.com.ja.controller;

import br.com.ja.model.OficialJustica;
import br.com.ja.persistence.repositories.OficialJusticaRepository;
import br.com.ja.persistence.service.OficialJusticaService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@RestController
@RequestMapping("/oficial-justica")
public class OficialJusticaController extends AbstractController<OficialJustica, OficialJusticaRepository, OficialJusticaService> {
  @Autowired
  private OficialJusticaRepository repository;
  @Autowired
  private OficialJusticaService service;
  @Autowired
  private ObjectMapper om;

  @Override
  protected OficialJusticaRepository getRepository() {
    return repository;
  }

  @Override
  protected OficialJusticaService getService() {
    return service;
  }

  @Override
  protected OficialJustica newInstance() {
    return OficialJustica.builder()
        .build();
  }

  @Override
  protected ObjectMapper om() {
    return om;
  }

  @Override
  protected Class<OficialJustica> clazz() {
    return OficialJustica.class;
  }

  @Override
  protected Specification<OficialJustica> findAllSpecification(HttpServletRequest request, String search) {
    return (root, query, builder) -> {
      String sLike = "%" + Optional.ofNullable(search).orElse("") + "%";
      return builder.and(
          builder.isFalse(builder.coalesce(root.get("deleted"), false)),
          builder.like(builder.function("unaccent", String.class, builder.lower(root.get("nome"))), sLike),
          builder.like(builder.function("unaccent", String.class, builder.lower(root.get("email"))), sLike)
      );
    };
  }
}
