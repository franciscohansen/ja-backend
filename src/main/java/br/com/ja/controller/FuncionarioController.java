package br.com.ja.controller;

import br.com.ja.model.Funcionario;
import br.com.ja.persistence.repositories.FuncionarioRepository;
import br.com.ja.persistence.service.FuncionarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.criteria.Predicate;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/funcionario")
public class FuncionarioController extends AbstractController<Funcionario, FuncionarioRepository, FuncionarioService> {

  @Autowired
  private FuncionarioRepository repository;
  @Autowired
  private FuncionarioService service;
  @Autowired
  private ObjectMapper om;

  @Override
  protected FuncionarioRepository getRepository() {
    return repository;
  }

  @Override
  protected FuncionarioService getService() {
    return service;
  }

  @Override
  protected Funcionario newInstance() {
    return Funcionario.builder()
        .build();
  }

  @Override
  protected ObjectMapper om() {
    return om;
  }

  @Override
  protected Class<Funcionario> clazz() {
    return Funcionario.class;
  }

  @Override
  protected Specification<Funcionario> findAllSpecification(HttpServletRequest request, String search) {
    return (root, query, builder) -> {
      String s = "%" + search.toLowerCase() + "%";
      Predicate where = builder.isFalse(builder.coalesce(root.get("deleted"), false));
      where = builder.and(
          where,
          builder.like(builder.function("unaccent", String.class, builder.lower(root.get("nome"))), s
          )
      );
      return where;
    };
  }
}
