package br.com.ja.controller;

import br.com.ja.model.Relatorio;
import br.com.ja.persistence.repositories.RelatorioRepository;
import br.com.ja.persistence.service.RelatorioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;

import javax.servlet.http.HttpServletRequest;

public class RelatorioController extends AbstractController<Relatorio, RelatorioRepository, RelatorioService> {

  private RelatorioRepository repository;
  private RelatorioService service;
  private ObjectMapper mapper;

  @Autowired
  public RelatorioController(RelatorioRepository repository, RelatorioService service, ObjectMapper mapper) {
    this.repository = repository;
    this.service = service;
    this.mapper = mapper;
  }

  @Override
  protected RelatorioRepository getRepository() {
    return repository;
  }

  @Override
  protected RelatorioService getService() {
    return service;
  }

  @Override
  protected Relatorio newInstance() {
    return Relatorio.builder().build();
  }

  @Override
  protected ObjectMapper om() {
    return mapper;
  }

  @Override
  protected Class<Relatorio> clazz() {
    return Relatorio.class;
  }

  @Override
  protected Specification<Relatorio> findAllSpecification(HttpServletRequest request, String search) {
    return null;
  }
}
