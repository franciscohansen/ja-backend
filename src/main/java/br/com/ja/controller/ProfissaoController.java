package br.com.ja.controller;

import br.com.ja.model.Profissao;
import br.com.ja.persistence.repositories.ProfissaoRepository;
import br.com.ja.persistence.service.ProfissaoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.criteria.Predicate;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/profissao")
public class ProfissaoController extends AbstractController<Profissao, ProfissaoRepository, ProfissaoService> {

  @Autowired
  private ProfissaoRepository repository;
  @Autowired
  private ProfissaoService service;
  @Autowired
  private ObjectMapper om;

  @Override
  protected ProfissaoRepository getRepository() {
    return repository;
  }

  @Override
  protected ProfissaoService getService() {
    return service;
  }

  @Override
  protected Profissao newInstance() {
    return Profissao.builder().build();
  }

  @Override
  protected ObjectMapper om() {
    return om;
  }

  @Override
  protected Class<Profissao> clazz() {
    return Profissao.class;
  }

  @Override
  protected Specification<Profissao> findAllSpecification(HttpServletRequest request, String search) {
    return (root, query, builder) -> {
      String s = "%" + search.toLowerCase() + "%";
      Predicate where = builder.isFalse(builder.coalesce(root.get("deleted"), false));
      where = builder.and(
          where,
          builder.like(builder.function("unaccent", String.class, builder.lower(root.get("descricao"))), s
          )
      );
      return where;
    };
  }
}
