package br.com.ja.controller;

import br.com.ja.model.Documento;
import br.com.ja.persistence.repositories.DocumentoRepository;
import br.com.ja.persistence.service.DocumentoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@RestController
@RequestMapping("/documentos")
public class DocumentosController extends AbstractController<Documento, DocumentoRepository, DocumentoService> {
  @Autowired
  private DocumentoRepository repository;
  @Autowired
  private DocumentoService service;
  @Autowired
  private ObjectMapper om;

  @Override
  protected DocumentoRepository getRepository() {
    return repository;
  }

  @Override
  protected DocumentoService getService() {
    return service;
  }

  @Override
  protected Documento newInstance() {
    return Documento.builder().build();
  }

  @Override
  protected ObjectMapper om() {
    return om;
  }

  @Override
  protected Class<Documento> clazz() {
    return Documento.class;
  }

  @Override
  protected Specification<Documento> findAllSpecification(HttpServletRequest request, String search) {
    return (root, query, builder) -> {
      String s = Optional.ofNullable(search).orElse("").toLowerCase();
      return builder.and(
          builder.isFalse(builder.coalesce(root.get("deleted"), false)),
          builder.like(builder.function("unaccent", String.class, builder.lower(root.get("descricao"))), "%" + s + "%")
      );
    };
  }
}
