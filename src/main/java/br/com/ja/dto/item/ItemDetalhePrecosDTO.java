package br.com.ja.dto.item;

import br.com.ja.dto.AbstractDTO;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ItemDetalhePrecosDTO extends AbstractDTO {


  private Double precoVenda;
  private Double precoCusto;

}
