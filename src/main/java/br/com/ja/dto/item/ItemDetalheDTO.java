package br.com.ja.dto.item;

import br.com.ja.dto.AbstractDTO;
import lombok.*;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ItemDetalheDTO extends AbstractDTO {

  private AbstractDTO empresa;
  private ItemDetalhePrecosDTO precos;

}
