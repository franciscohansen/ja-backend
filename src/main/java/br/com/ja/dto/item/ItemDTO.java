package br.com.ja.dto.item;

import br.com.ja.dto.AbstractDTO;
import lombok.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ItemDTO extends AbstractDTO {

  private List<ItemDetalheDTO> detalhes;

}
