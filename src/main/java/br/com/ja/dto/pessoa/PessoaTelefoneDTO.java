package br.com.ja.dto.pessoa;

import br.com.ja.dto.AbstractDTO;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PessoaTelefoneDTO extends AbstractDTO {
  private Boolean padrao;
  private String tipo;
  private String ddd;
  private String numero;
  private String ramal;
  private String observacoes;
}
