package br.com.ja.dto.pessoa;

import br.com.ja.dto.AbstractDTO;
import lombok.*;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PessoaVeiculoDTO extends AbstractDTO {
  private String marca;
  private String descricao;
  private String placa;
  private String renavam;
  private String ano;
  private String cor;
}
