package br.com.ja.dto.pessoa;

import br.com.ja.dto.AbstractDTO;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PessoaClienteDTO extends AbstractDTO {

  private String naturalDe;
  private PessoaClienteFiliacaoDTO filiacao;
}
