package br.com.ja.dto.pessoa;

import br.com.ja.dto.AbstractDTO;
import br.com.ja.model.Endereco;
import br.com.ja.model.cliente.Cliente;
import br.com.ja.model.cliente.ClienteVeiculo;
import br.com.ja.model.enums.EEstadoCivil;
import br.com.ja.model.enums.ESexo;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PessoaDTO extends AbstractDTO {
  private AbstractDTO empresa;
  private List<AbstractDTO> empresas;
  private String tipoPessoa;
  private String nome;
  private String razaoSocial;
  private String cpfCnpj;
  private String rgIe;
  private String email;
  private Endereco endereco;
  private Endereco enderecoEntrega;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
  private Date dataNascimentoFundacao;

  private PessoaClienteDTO dadosCliente;
  private PessoaFornecedorDTO dadosFornecedor;
  private List<PessoaVeiculoDTO> veiculos;


  private ESexo sexo;
  private EEstadoCivil estadoCivil;

  private List<PessoaTelefoneDTO> telefones;

  private Boolean cliente;
  private Boolean fornecedor;

  public void addVeiculos(List<PessoaVeiculoDTO> veiculos) {
    if (this.veiculos == null) {
      this.veiculos = new ArrayList<>();
    }
    this.veiculos.addAll(veiculos);
  }

  public void addTelefones(List<PessoaTelefoneDTO> telefones) {
    if (this.telefones == null) {
      this.telefones = new ArrayList<>();
    }

    this.telefones.addAll(telefones);
  }

  public static PessoaDTO fromCliente(Cliente cliente) {
    PessoaDTO dto = PessoaDTO.builder()
        .cliente(true)
        .fornecedor(true)
        .empresa(new AbstractDTO(1L))
        .empresas(Collections.singletonList(new AbstractDTO(1L)))
        .tipoPessoa(cliente.isPessoaJuridica() ? "JURIDICA" : "FISICA")
        .nome(cliente.getNome())
        .razaoSocial(cliente.getRazaoSocial())
        .cpfCnpj(cliente.getCpfCnpj())
        .rgIe(cliente.getRgIe())
        .dataNascimentoFundacao(cliente.getDataNascimento())
        .email(cliente.getEmail())
        .estadoCivil(cliente.getEstadoCivil())
        .endereco(cliente.getEndereco())
        .enderecoEntrega(cliente.getEnderecoCarne())
        .sexo(cliente.getSexo())
        .dadosCliente(buildDadosCliente(cliente))
        .dadosFornecedor(buildDadosFornecedor(cliente))
        .veiculos(buildVeiculos(cliente))
        .build();
    if (Optional.ofNullable(cliente.getIdHabil()).orElse(0L) > 0L) {
      dto.setId(cliente.getIdHabil());
    }
    dto.setTelefones(buildTelefones(cliente));
    return dto;
  }

  public PessoaDTO fillFromCliente(Cliente cliente) {
    nome = cliente.getNome();
    razaoSocial = cliente.getRazaoSocial();
    cpfCnpj = cliente.getCpfCnpj();
    rgIe = cliente.getRgIe();
    dataNascimentoFundacao = cliente.getDataNascimento();
    email = cliente.getEmail();
    estadoCivil = cliente.getEstadoCivil();
    endereco = cliente.getEndereco();
    enderecoEntrega = cliente.getEnderecoCarne();
    sexo = cliente.getSexo();
    dadosCliente = buildDadosCliente(cliente);
    dadosFornecedor = buildDadosFornecedor(cliente);
    addVeiculos(buildVeiculos(cliente));
    addTelefones(buildTelefones(cliente));
    return this;
  }

  private static List<PessoaVeiculoDTO> buildVeiculos(Cliente cliente) {
    List<PessoaVeiculoDTO> veiculos = new ArrayList<>();
    if (cliente.getVeiculos() != null && !cliente.getVeiculos().isEmpty()) {
      for (ClienteVeiculo veiculo : cliente.getVeiculos()) {
        veiculos.add(
            PessoaVeiculoDTO.builder()
                .marca(veiculo.getMarca())
                .ano(veiculo.getAno())
                .cor(veiculo.getCor())
                .descricao(veiculo.getModelo())
                .placa(veiculo.getPlaca())
                .renavam(veiculo.getRenavam())
                .build()
        );
      }
    }
    return veiculos;
  }

  private static PessoaFornecedorDTO buildDadosFornecedor(Cliente cliente) {
    return PessoaFornecedorDTO.builder()
        .agencia(cliente.getAgencia())
        .conta(cliente.getContaBancaria())
        .build();
  }

  private static PessoaClienteDTO buildDadosCliente(Cliente cliente) {
    return PessoaClienteDTO.builder()
        .naturalDe(cliente.getNaturalidade())
        .filiacao(
            PessoaClienteFiliacaoDTO.builder()
                .nomeMae(cliente.getNomeMae())
                .nomePai(cliente.getNomePai())
                .build()
        )
        .build();
  }

  private static List<PessoaTelefoneDTO> buildTelefones(Cliente cliente) {
    List<PessoaTelefoneDTO> telefones = new ArrayList<>();
    if (cliente.getTelefones() != null) {
      cliente.getTelefones().forEach(
          f -> {
            PessoaTelefoneDTO foneDto = PessoaTelefoneDTO.builder()
                .padrao(f.getPadrao())
                .tipo(f.getTipo())
                .ddd(f.getDdd())
                .numero(f.getFone())
                .ramal(f.getRamal())
                .observacoes(f.getObservacoes())
                .build();
            foneDto.setId(f.getIdHabil());
            telefones.add(foneDto);
          }
      );
    }
    return telefones;
  }


}
