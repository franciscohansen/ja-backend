package br.com.ja.dto.pessoa;

import br.com.ja.dto.AbstractDTO;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PessoaClienteFiliacaoDTO extends AbstractDTO {
  private String nomePai;
  private String nomeMae;
}
