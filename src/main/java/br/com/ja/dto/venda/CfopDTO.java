package br.com.ja.dto.venda;

import br.com.ja.dto.AbstractDTO;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CfopDTO extends AbstractDTO {
  private String codigo;
}
