package br.com.ja.dto.venda;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VendaDetalheRefDetalheDTO implements Serializable {
  private long idDetalhe;
  @Builder.Default
  private String tipo = "DETALHE";
}
