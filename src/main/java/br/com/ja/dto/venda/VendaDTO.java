package br.com.ja.dto.venda;

import br.com.ja.dto.AbstractDTO;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.Date;
import java.util.List;

@Builder(access = AccessLevel.PUBLIC)
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class VendaDTO extends AbstractDTO {

  private CfopDTO cfop;
  private String tipo;
  private String subTipo;
  private AbstractDTO pessoa;
  private AbstractDTO vendedor;
  private AbstractDTO empresa;
  private VendaInfoPessoaDTO infoPessoa;
  private VendaInfoNFeDTO infoNFe;
  private VendaTotaisDTO totais;
  private AbstractDTO centroCusto;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
  private Date dataEmissao;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
  private Date dataCadastro;
  private Long referencia;
  private List<VendaDetalheDTO> detalhes;
  private String observacoes;


}
