package br.com.ja.dto.venda;

import br.com.ja.dto.AbstractDTO;
import lombok.*;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class VendaDetalheDTO extends AbstractDTO {
  private AbstractDTO item;
  private String descricaoProduto;
  private Double qtde;
  private Double preco;

  private Double total;
  private AbstractDTO vendedor;
  private VendaDetalheRefDetalheDTO itemRefDetalhe;
}
