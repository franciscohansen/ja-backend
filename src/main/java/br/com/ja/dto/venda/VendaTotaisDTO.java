package br.com.ja.dto.venda;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VendaTotaisDTO {
  private Double total;
  private Double totalProdutos;
}
