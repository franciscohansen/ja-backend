package br.com.ja.dto.venda;

import br.com.ja.model.Endereco;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VendaInfoPessoaDTO {
  private String nomePessoa;
  private String cpfCnpj;
  private String rgIe;
  private Endereco endereco;
}
