package br.com.ja.dto.conta;

import br.com.ja.dto.AbstractDTO;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.Date;
import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ContaDTO extends AbstractDTO {
  private AbstractDTO empresa;
  private AbstractDTO pessoa;
  private AbstractDTO vendedor;
  private String descricao;
  private String documento;
  private Boolean cancelado;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
  private Date cadastro;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
  private Date vencimento;
  private Long referencia;
  @Builder.Default
  private String pedeDados = "CREDIARIO";
  @Builder.Default
  private String tipo = "RECEBIMENTO";
  private List<ContaDetalheDTO> detalhes;
}
