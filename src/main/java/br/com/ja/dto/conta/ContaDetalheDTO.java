package br.com.ja.dto.conta;

import br.com.ja.dto.AbstractDTO;
import lombok.*;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ContaDetalheDTO extends AbstractDTO {
  private AbstractDTO centroCusto;
  private AbstractDTO contaCaixa;
  private Double valor;
}
