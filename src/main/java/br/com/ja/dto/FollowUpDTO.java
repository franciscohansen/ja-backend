package br.com.ja.dto;

import br.com.ja.dto.pessoa.PessoaDTO;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.Date;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class FollowUpDTO extends AbstractDTO {

  private AbstractDTO empresa;
  private PessoaDTO cliente;
  private PessoaDTO funcionario;
  private String motivo;
  private String tipo;
  private String assunto;
  private String situacao;
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
  private Date dataCadastro;
  private String resumoF;
}
