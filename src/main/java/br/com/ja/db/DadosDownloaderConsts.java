package br.com.ja.db;

public class DadosDownloaderConsts {

  private DadosDownloaderConsts(){

  }
  public static final String CLIENTE = "SELECT \"Codigo\"                   as id,\n" +
      "false                        as deleted,\n" +
      "\"Empresa\"                  as id_empresa,\n" +
      "\"Codigo\"                   as id_habil,\n" +
      "\"Cliente_CPF\"              as cpfCnpj,\n" +
      "\"Cliente_Data_Nascimento\"  as dataNascimento,\n" +
      "\"Outros_Email\"             as email,\n" +
      "\"Cliente_Bairro_Descrição\" as endereco_bairro,\n" +
      "\"Cliente_CEP\"              as endereco_cep,\n" +
      "\"Cliente_Referência\"       as endereco_complemento,\n" +
      "\"Cliente_Endereço\"         as endereco_logradouro,\n" +
      "\"Cliente_Cidade\"           as endereco_municipio,\n" +
      "nro                          as endereco_numero,\n" +
      "'Brasil'                     as endereco_pais,\n" +
      "\"Cliente_UF\"               as endereco_uf,\n" +
      "\"Entrega_Bairro_Descrição\" as endereco_carne_bairro,\n" +
      "\"Entrega_CEP\"              as endereco_carne_cep,\n" +
      "\"Entrega_Referência\"       as endereco_carne_complemento,\n" +
      "\"Entrega_Endereço\"         as endereco_carne_logradouro,\n" +
      "\"Entrega_Cidade\"           as endereco_carne_municipio,\n" +
      "\"Entrega_Numero\"           as endereco_carne_numero,\n" +
      "'Brasil'                   as endereco_carne_pais,\n" +
      "\"Entrega_UF\"               as endereco_carne_uf,\n" +
      "CASE\n" +
      "    WHEN \"Cliente_Estado_Civil\" = 'S' THEN 'SOLTEIRO'\n" +
      "    WHEN \"Cliente_Estado_Civil\" = 'C' THEN 'CASADO'\n" +
      "    WHEN \"Cliente_Estado_Civil\" = 'D' THEN 'DESQUITADO'\n" +
      "    WHEN \"Cliente_Estado_Civil\" = 'A' THEN 'AMASIADO'\n" +
      "    WHEN \"Cliente_Estado_Civil\" = 'O' THEN 'OUTRO'\n" +
      "    WHEN \"Cliente_Estado_Civil\" = 'V' THEN 'VIUVO'\n" +
      "    ELSE 'SOLTEIRO'\n" +
      "    END                      as estado_civil,\n" +
      "\"Cliente_Natural\"          as naturalidade,\n" +
      "\"Cliente_Nome\"             as nome,\n" +
      "\"Pais_Nome_Mãe\"               nome_mae,\n" +
      "\"Pais_Nome_Pai\"            as nome_pai,\n" +
      "\"Outros_Pessoa_Juridica\"   as pessoa_juridica,\n" +
      "\"Cliente_Razão_Social\"     as razao_social,\n" +
      "\"Cliente_RG\"               as rg_ie,\n" +
      "CASE\n" +
      "           WHEN \"Cliente_Sexo\" = 'M' THEN 'MASCULINO'\n" +
      "           WHEN \"Cliente_Sexo\" = 'F' THEN 'FEMININO'\n" +
      "           ELSE 'MASCULINO'\n" +
      "           END                    as sexo,\n" +
      "null                       as id_profissao\n" +
      "FROM \"Cliente\"\n" +
      "WHERE \"Empresa\" = 1\n" +
      "ORDER BY \"Codigo\"\n";

}
