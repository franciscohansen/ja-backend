package br.com.ja.db;

import br.com.ja.model.cliente.Cliente;
import br.com.ja.persistence.repositories.ClienteRepository;
import br.com.ja.persistence.repositories.FuncionarioRepository;
import br.com.ja.persistence.repositories.ProfissaoRepository;
import br.com.ja.persistence.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static br.com.ja.util.Constants.NOT_IMPLEMENTED;

@Component
public class DadosDownloader {
  @PersistenceContext
  private EntityManager em;

  @Autowired
  private ClienteRepository clienteRepository;
  @Autowired
  private FuncionarioRepository funcionarioRepository;
  @Autowired
  private ProfissaoRepository profissaoRepository;
  @Autowired
  private UsuarioService usuarioService;

  public void doDownloadData() {
    String token = usuarioService.token();
  }

  private DriverManagerDataSource ds() {
    DriverManagerDataSource ds = new DriverManagerDataSource("jdbc:postgresql://localhost:4567/tn12466720000126");
    ds.setUsername("habilx");
    ds.setDriverClassName("org.postgresql.Driver");
    ds.setPassword("koihw2017");
    return ds;
  }

  @Transactional
  public void downloadCliente(String token) {
    try (Connection conn = ds().getConnection();
         PreparedStatement stmt = conn.prepareStatement(DadosDownloaderConsts.CLIENTE);
         ResultSet rs = stmt.executeQuery();
    ) {
      List<Cliente> clientes = new ArrayList<>();
      while (rs.next()) {


      }
    } catch (SQLException e) {
      e.printStackTrace();
    }


  }

  @Transactional
  public void downloadFuncionario(String token) {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  @Transactional
  public void downloadProfissao(String token) {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  public void downloadEmpresa(String token) {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }


}
