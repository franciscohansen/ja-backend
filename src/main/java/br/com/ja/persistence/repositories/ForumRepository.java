package br.com.ja.persistence.repositories;

import br.com.ja.model.Forum;
import org.springframework.stereotype.Repository;

@Repository
public interface ForumRepository extends IRepository<Forum> {
}
