package br.com.ja.persistence.repositories;

import br.com.ja.model.FollowUp;
import br.com.ja.projections.FollowUpMotivoProjection;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FollowUpRepository extends IRepository<FollowUp> {
  String DISTINCT_MOTIVO_QUERY = "SELECT DISTINCT upper(motivo) as motivo " +
      "FROM follow_up " +
      "WHERE motivo is not null " +
      "ORDER BY upper(motivo)";

  List<FollowUp> findAllByJuridicoIdOrderByIdDesc(Long id);

  @Query(nativeQuery = true, value = DISTINCT_MOTIVO_QUERY)
  List<FollowUpMotivoProjection> findAllMotivos();


}
