package br.com.ja.persistence.repositories;

import br.com.ja.model.Configuracao;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ConfiguracaoRepository extends IRepository<Configuracao> {

  Optional<Configuracao> findFirstByOrderByIdDesc();
}
