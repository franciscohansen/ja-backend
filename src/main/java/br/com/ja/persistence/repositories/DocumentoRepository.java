package br.com.ja.persistence.repositories;

import br.com.ja.model.Documento;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentoRepository extends IRepository<Documento> {
}
