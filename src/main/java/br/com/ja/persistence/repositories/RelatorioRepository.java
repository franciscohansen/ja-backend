package br.com.ja.persistence.repositories;

import br.com.ja.model.Relatorio;

public interface RelatorioRepository extends IRepository<Relatorio> {
}
