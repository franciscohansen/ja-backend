package br.com.ja.persistence.repositories;

import br.com.ja.model.Profissao;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfissaoRepository extends IRepository<Profissao> {
}
