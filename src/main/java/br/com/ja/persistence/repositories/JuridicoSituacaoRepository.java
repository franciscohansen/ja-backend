package br.com.ja.persistence.repositories;

import br.com.ja.model.juridico.situacao.JuridicoSituacao;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JuridicoSituacaoRepository extends IRepository<JuridicoSituacao> {

  List<JuridicoSituacao> findAllByJuridicoId(Long id);

}
