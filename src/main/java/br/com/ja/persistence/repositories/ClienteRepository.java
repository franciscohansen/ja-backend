package br.com.ja.persistence.repositories;

import br.com.ja.model.cliente.Cliente;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends IRepository<Cliente> {

  boolean existsByCpfCnpj(String cpfCnpj);

  Cliente getByCpfCnpj(String cpfCnpj);

  Cliente findFirstByCpfCnpj(String cpfCnpj);

}
