package br.com.ja.persistence.repositories;

import br.com.ja.model.juridico.JuridicoVisualizacao;
import org.springframework.stereotype.Repository;

@Repository
public interface JuridicoVisualizacaoRepository extends IRepository<JuridicoVisualizacao> {
}
