package br.com.ja.persistence.repositories;

import br.com.ja.model.juridico.Juridico;
import org.springframework.stereotype.Repository;

@Repository
public interface JuridicoRepository extends IRepository<Juridico> {
}
