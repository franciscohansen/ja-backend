package br.com.ja.persistence.repositories;

import br.com.ja.model.AbstractModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;

@NoRepositoryBean
public interface IRepository<T extends AbstractModel> extends JpaRepository<T, Long>, JpaSpecificationExecutor<T> {
  Optional<T> findByIdHabil(Long idHabil);

  T getByIdHabil(long idHabil);

  boolean existsByIdHabil(Long idHabil);
}
