package br.com.ja.persistence.repositories;

import br.com.ja.model.Report;

public interface ReportRepository extends IRepository<Report>{
}
