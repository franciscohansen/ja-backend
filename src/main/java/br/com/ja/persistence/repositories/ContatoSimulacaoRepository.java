package br.com.ja.persistence.repositories;

import br.com.ja.model.contatos.ContatoSimulacao;
import org.springframework.stereotype.Repository;

@Repository
public interface ContatoSimulacaoRepository extends IRepository<ContatoSimulacao> {
}
