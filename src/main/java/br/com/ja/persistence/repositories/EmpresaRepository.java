package br.com.ja.persistence.repositories;

import br.com.ja.model.Empresa;
import org.springframework.stereotype.Repository;

@Repository
public interface EmpresaRepository extends IRepository<Empresa> {
}
