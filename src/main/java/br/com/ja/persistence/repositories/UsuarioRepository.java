package br.com.ja.persistence.repositories;

import br.com.ja.model.Usuario;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends IRepository<Usuario> {
}
