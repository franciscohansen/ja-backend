package br.com.ja.persistence.repositories;

import br.com.ja.model.Banco;
import org.springframework.stereotype.Repository;

@Repository
public interface BancoRepository extends IRepository<Banco> {
}
