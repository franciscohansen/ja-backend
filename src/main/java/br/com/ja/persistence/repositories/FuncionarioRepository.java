package br.com.ja.persistence.repositories;

import br.com.ja.model.Funcionario;
import org.springframework.stereotype.Repository;

@Repository
public interface FuncionarioRepository extends IRepository<Funcionario> {
}
