package br.com.ja.persistence.repositories;

import br.com.ja.model.OficialJustica;
import org.springframework.stereotype.Repository;

@Repository
public interface OficialJusticaRepository extends IRepository<OficialJustica> {
}
