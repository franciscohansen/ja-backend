package br.com.ja.persistence.sync.persisters.ja.profissao;

import br.com.ja.model.Profissao;
import br.com.ja.persistence.sync.common.IDataSourceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static br.com.ja.persistence.sync.persisters.ja.profissao.ProfissaoDataPersisterConst.JA_PROFISSAO_SQL;

@Component
public class ProfissaoDataPersister {

  private static final Logger LOGGER = LoggerFactory.getLogger(ProfissaoDataPersister.class);

  @Autowired
  @Qualifier("ja-provider")
  private IDataSourceProvider provider;

  @Transactional
  public void doPersistProfissoes(List<Profissao> profissoes) {
    try (Connection conn = this.provider.dataSource().getConnection();
         PreparedStatement stmt = conn.prepareStatement(JA_PROFISSAO_SQL)) {
      conn.setAutoCommit(false);
      for (Profissao profissao : profissoes) {
        this.fillStmt(stmt, profissao);
        stmt.addBatch();
      }
      stmt.executeBatch();
      conn.commit();
    } catch (SQLException e) {
      LOGGER.error(e.getMessage(), e);
    }
  }

  private void fillStmt(PreparedStatement stmt, Profissao profissao) throws SQLException {
    stmt.setBoolean(1, profissao.getDeleted());
    stmt.setLong(2, profissao.getIdEmpresa());
    stmt.setLong(3, profissao.getIdHabil());
    stmt.setString(4, profissao.getDescricao().toUpperCase());
  }
}
