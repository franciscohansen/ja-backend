package br.com.ja.persistence.sync.persisters.ja.cliente;

public class ClienteDataPersisterConsts {

  private ClienteDataPersisterConsts() {
  }

  public static final String JA_CLIENTE_SQL = "INSERT INTO public.cliente(deleted, id_empresa, id_habil, cpf_cnpj, data_nascimento, email, endereco_bairro,\n" +
      "                           endereco_cep, endereco_complemento, endereco_logradouro, endereco_municipio, endereco_numero,\n" +
      "                           endereco_pais, endereco_uf, endereco_carne_bairro, endereco_carne_cep,\n" +
      "                           endereco_carne_complemento, endereco_carne_logradouro, endereco_carne_municipio,\n" +
      "                           endereco_carne_numero, endereco_carne_pais, endereco_carne_uf, estado_civil, naturalidade,\n" +
      "                           nome, nome_mae, nome_pai, pessoa_juridica, razao_social, rg_ie, sexo, id_profissao)\n" +
      "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)\n" +
      "ON CONFLICT (id_habil, cpf_cnpj)\n" +
      "    do update set data_nascimento=excluded.data_nascimento,\n" +
      "                  email=excluded.email,\n" +
      "                  endereco_bairro=excluded.endereco_bairro,\n" +
      "                  endereco_cep=excluded.endereco_cep,\n" +
      "                  endereco_complemento=excluded.endereco_complemento,\n" +
      "                  endereco_logradouro=excluded.endereco_logradouro,\n" +
      "                  endereco_municipio=excluded.endereco_municipio,\n" +
      "                  endereco_numero=excluded.endereco_numero,\n" +
      "                  endereco_pais=excluded.endereco_pais,\n" +
      "                  endereco_uf=excluded.endereco_uf,\n" +
      "                  endereco_carne_bairro=excluded.endereco_carne_bairro,\n" +
      "                  endereco_carne_cep=excluded.endereco_carne_cep,\n" +
      "                  endereco_carne_complemento=excluded.endereco_carne_complemento,\n" +
      "                  endereco_carne_logradouro=excluded.endereco_carne_logradouro,\n" +
      "                  endereco_carne_municipio=excluded.endereco_carne_municipio,\n" +
      "                  endereco_carne_numero=excluded.endereco_carne_numero,\n" +
      "                  endereco_carne_pais=excluded.endereco_carne_pais,\n" +
      "                  endereco_carne_uf=excluded.endereco_carne_uf,\n" +
      "                  estado_civil=excluded.estado_civil,\n" +
      "                  naturalidade=excluded.naturalidade,\n" +
      "                  nome=excluded.nome,\n" +
      "                  nome_mae=excluded.nome_mae,\n" +
      "                  nome_pai=excluded.nome_pai,\n" +
      "                  pessoa_juridica=excluded.pessoa_juridica,\n" +
      "                  razao_social=excluded.razao_social,\n" +
      "                  rg_ie=excluded.rg_ie,\n" +
      "                  sexo=excluded.sexo WHERE not COALESCE(excluded.has_new_data, false);";

  public static final String JA_CLIENTE_FONE_SQL = "insert into cliente_telefones (deleted, id_empresa, id_habil, ddd, fone, observacoes, padrao, ramal, tipo, id_cliente ) values " +
      "(?,?,?,?,?,?,?,?,?, (select id from cliente where id_habil=? limit 1))" +
      " ON CONFLICT (id_empresa, id_habil) DO UPDATE SET " +
      "deleted=excluded.deleted," +
      "ddd=excluded.ddd," +
      "fone=excluded.fone," +
      "observacoes=excluded.observacoes," +
      "padrao=excluded.padrao," +
      "ramal=excluded.ramal," +
      "tipo=excluded.tipo," +
      "id_cliente=excluded.id_cliente";

}
