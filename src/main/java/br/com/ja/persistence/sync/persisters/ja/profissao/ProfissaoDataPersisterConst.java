package br.com.ja.persistence.sync.persisters.ja.profissao;

public class ProfissaoDataPersisterConst {
  public static final String JA_PROFISSAO_SQL = "INSERT INTO PROFISSAO (deleted, id_empresa, id_habil, descricao) " +
      "VALUES (?,?,?,?) " +
      "ON CONFLICT (id_habil, UPPER(descricao)) DO " +
      "UPDATE set deleted=excluded.deleted," +
      "id_empresa=excluded.id_empresa," +
      "descricao=UPPER(excluded.descricao)";
}
