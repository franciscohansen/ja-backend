package br.com.ja.persistence.sync.persisters.ja.cliente;

import br.com.ja.model.Endereco;
import br.com.ja.model.cliente.Cliente;
import br.com.ja.model.cliente.ClienteTelefones;
import br.com.ja.model.enums.EEstadoCivil;
import br.com.ja.model.enums.ESexo;
import br.com.ja.persistence.sync.common.IDataSourceProvider;
import br.com.ja.util.FormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.time.ZoneId;
import java.util.List;
import java.util.Optional;

import static br.com.ja.persistence.sync.persisters.ja.cliente.ClienteDataPersisterConsts.JA_CLIENTE_FONE_SQL;
import static br.com.ja.persistence.sync.persisters.ja.cliente.ClienteDataPersisterConsts.JA_CLIENTE_SQL;

@Component
public class ClienteDataPersister {

  private static final Logger LOGGER = LoggerFactory.getLogger(ClienteDataPersister.class);
  private static final int BATCH_SIZE = 1000;

  @Autowired
  @Qualifier("ja-provider")
  private IDataSourceProvider provider;


  @Transactional
  public void doPersistClientes(List<Cliente> clientes) {
    try (Connection conn = this.provider.dataSource().getConnection();
         PreparedStatement stmt = conn.prepareStatement(JA_CLIENTE_SQL)) {
      conn.setAutoCommit(false);
      int count = 0;
      for (Cliente cliente : clientes) {
        this.fillStmt(stmt, cliente);
        stmt.addBatch();
        count++;
        if (count % BATCH_SIZE == 0) {
          stmt.executeBatch();
          conn.commit();
        }
      }
      stmt.executeBatch();
      conn.commit();
    } catch (SQLException e) {
      LOGGER.error(e.getMessage(), e);
    }
  }

  private void fillStmt(PreparedStatement stmt, Cliente cliente) throws SQLException {
    String cpf = FormatUtils.getFormattedDoc(cliente.getCpfCnpj());
    stmt.setBoolean(1, false);//deleted
    stmt.setLong(2, 1);//id_empresa
    stmt.setLong(3, cliente.getIdHabil());//id_habil
    stmt.setString(4, cpf);//cpf_cnpj
    stmt.setObject(5, cliente.getDataNascimento() != null ? cliente.getDataNascimento().toInstant().atZone(ZoneId.of("America/Sao_Paulo")).toLocalDateTime() : null);//data_nascimento
    stmt.setString(6, cliente.getEmail());//email
    this.fillEndereco(stmt, cliente);
    this.fillEnderecoCarne(stmt, cliente);
    stmt.setString(23, Optional.ofNullable(cliente.getEstadoCivil()).orElse(EEstadoCivil.NAO_SE_APLICA).name());//estado_civil,
    stmt.setString(24, cliente.getNaturalidade());//naturalidade,
    stmt.setString(25, cliente.getNome());//nome,
    stmt.setString(26, cliente.getNomeMae());//nome_mae,
    stmt.setString(27, cliente.getNomeMae());//nome_pai,
    stmt.setBoolean(28, cliente.isPessoaJuridica());//pessoa_juridica,
    stmt.setString(29, cliente.getRazaoSocial());//razao_social,
    stmt.setString(30, cliente.getRgIe());//rg_ie,
    stmt.setString(31, Optional.ofNullable(cliente.getSexo()).orElse(ESexo.NAO_SE_APLICA).name());//sexo,
    stmt.setNull(32, Types.BIGINT);//id_profissao
  }

  private void fillEndereco(PreparedStatement stmt, Cliente cliente) throws SQLException {
    Endereco endereco = cliente.getEndereco();
    stmt.setString(7, endereco.getBairro());//endereco_bairro
    stmt.setString(8, endereco.getCep());//endereco_cep
    stmt.setString(9, endereco.getComplemento());//endereco_complemento
    stmt.setString(10, endereco.getLogradouro());//endereco_logradouro,
    stmt.setString(11, endereco.getMunicipio());//endereco_municipio,
    stmt.setString(12, endereco.getNumero());//endereco_numero,
    stmt.setString(13, endereco.getPais());//endereco_pais,
    stmt.setString(14, endereco.getUf());//endereco_uf,
  }

  private void fillEnderecoCarne(PreparedStatement stmt, Cliente cliente) throws SQLException {
    Endereco carne = cliente.getEnderecoCarne();
    stmt.setString(15, carne.getBairro());//endereco_carne_bairro,
    stmt.setString(16, carne.getCep());//endereco_carne_cep,
    stmt.setString(17, carne.getComplemento());//endereco_carne_complemento,
    stmt.setString(18, carne.getLogradouro());//endereco_carne_logradouro,
    stmt.setString(19, carne.getMunicipio());//endereco_carne_municipio,
    stmt.setString(20, carne.getNumero());//endereco_carne_numero,
    stmt.setString(21, carne.getPais());//endereco_carne_pais,
    stmt.setString(22, carne.getUf());//endereco_carne_uf,
  }

  public void doPersistClientesFones(List<ClienteTelefones> fones) {
    try (Connection conn = this.provider.dataSource().getConnection();
         PreparedStatement stmt = conn.prepareStatement(JA_CLIENTE_FONE_SQL)) {
      conn.setAutoCommit(false);
      int count = 0;
      for (ClienteTelefones fone : fones) {
        this.fillFoneStmt(stmt, fone);
        stmt.addBatch();
        count++;
        if (count % BATCH_SIZE == 0) {
          stmt.executeBatch();
          conn.commit();
        }
      }
      stmt.executeBatch();
      conn.commit();
    } catch (SQLException e) {
      LOGGER.error(e.getMessage(), e);
    }
  }

  private void fillFoneStmt(PreparedStatement stmt, ClienteTelefones fone) throws SQLException {
    stmt.setBoolean(1, fone.getDeleted());//deleted
    stmt.setLong(2, 1);//id_empresa
    stmt.setLong(3, fone.getIdHabil());//id_habil
    stmt.setString(4, fone.getDdd());//ddd
    stmt.setObject(5, fone.getFone());//fone
    stmt.setString(6, fone.getObservacoes());//observacoes
    stmt.setBoolean(7, fone.getPadrao());//padrao
    stmt.setString(8, fone.getRamal());//ramal
    stmt.setString(9, fone.getTipo());//tipo
    stmt.setLong(10, fone.getCliente().getId());//id_cliente,
  }


}
