package br.com.ja.persistence.sync.persisters.h10.pessoa;

import br.com.ja.dto.pessoa.PessoaClienteDTO;
import br.com.ja.dto.pessoa.PessoaClienteFiliacaoDTO;
import br.com.ja.dto.pessoa.PessoaDTO;
import br.com.ja.dto.pessoa.PessoaFornecedorDTO;
import br.com.ja.model.Endereco;
import br.com.ja.persistence.sync.common.IDataSourceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;

@Component
public class PessoaPersister {
  private static final Logger LOGGER = LoggerFactory.getLogger(PessoaPersister.class);

  private IDataSourceProvider provider;

  @Autowired
  public PessoaPersister(@Qualifier("h10-provider") IDataSourceProvider h10Provider) {
    this.provider = h10Provider;
  }


  public boolean persistPessoa(PessoaDTO pessoa) {
    if (Optional.ofNullable(pessoa.getId()).orElse(0L) > 0) {

    } else {
      return updatePessoa(pessoa);
    }
    return true;
  }

  private boolean updatePessoa(PessoaDTO pessoa) {
    try (Connection conn = this.provider.dataSource().getConnection();
    ) {
      this.runPessoaUpdateSQL(pessoa, conn);
      this.runPessoaClienteUpdate(pessoa, conn);
      this.runPessoaFiliacaoUpdate(pessoa, conn);
      this.runPessoaFornecedorUpdate(pessoa, conn);
      return true;
    } catch (SQLException e) {
      LOGGER.error("Error Updating Pessoa: {}", e.getMessage(), e);
      return false;
    }
  }

  private void runPessoaUpdateSQL(PessoaDTO dto, Connection connection) throws SQLException {
    try (PreparedStatement stmt = connection.prepareStatement(PessoaPersisterUpdateConsts.H10_PESSOA_UPDATE_SQL)) {
      stmt.setString(1, dto.getNome());//nome
      stmt.setString(2, dto.getRazaoSocial());//razaosocial
      stmt.setString(3, dto.getTipoPessoa());//tipopessoa
      stmt.setString(4, dto.getCpfCnpj());//cpfcnpj
      stmt.setString(5, dto.getRgIe());//rgie
      stmt.setString(6, dto.getEmail());//email
      Endereco end = dto.getEndereco();
      stmt.setString(7, end.getBairro());//endbairro
      stmt.setString(8, end.getCep());//endcep
      stmt.setString(9, end.getComplemento());//endcomplemento
      stmt.setString(10, end.getLogradouro());//endlogradouro
      stmt.setString(11, end.getMunicipio());//endmunicipio
      stmt.setString(12, end.getNumero());//endnumero
      stmt.setString(13, end.getPais());//endpais
      stmt.setString(14, end.getUf());//enduf
      Endereco entr = dto.getEnderecoEntrega();
      stmt.setString(15, entr.getBairro());//entrbairro
      stmt.setString(16, entr.getCep());//entrcep
      stmt.setString(17, entr.getComplemento());//entrcomplemento
      stmt.setString(18, entr.getComplemento());//entrlogradouro
      stmt.setString(19, entr.getMunicipio());//entrmunicipio
      stmt.setString(20, entr.getNumero());//entrnumero
      stmt.setString(21, entr.getPais());//entrpais
      stmt.setString(22, entr.getUf());//entruf
      stmt.setString(23, dto.getSexo().name());//sexo
      stmt.setString(24, dto.getEstadoCivil().name());//estadocivil
      stmt.setBoolean(25, true);//cliente
      stmt.setBoolean(26, true);//fornecedor
      stmt.setDate(27, new java.sql.Date(dto.getDataNascimentoFundacao().getTime()));//datanascimentofundacao
      stmt.setLong(28, dto.getId());//id
      stmt.executeUpdate();
    }
  }

  private void runPessoaClienteUpdate(PessoaDTO dto, Connection conn) throws SQLException {
    if (dto.getDadosCliente() == null) {
      return;
    }
    try (PreparedStatement stmt = conn.prepareStatement(PessoaPersisterUpdateConsts.H10_PESSOA_CLIENTE_UPDATE_SQL)) {
      PessoaClienteDTO cliente = dto.getDadosCliente();
      stmt.setString(1, cliente.getNaturalDe());
      stmt.setLong(2, dto.getId());
      stmt.executeUpdate();
    }
  }

  private void runPessoaFiliacaoUpdate(PessoaDTO dto, Connection conn) throws SQLException {
    if (dto.getDadosCliente() == null || dto.getDadosCliente().getFiliacao() == null) {
      return;
    }
    try (PreparedStatement stmt = conn.prepareStatement(PessoaPersisterUpdateConsts.H10_PESSOA_CLIENTE_FILIACAO_UPDATE_SQL)) {
      PessoaClienteFiliacaoDTO filiacao = dto.getDadosCliente().getFiliacao();
      stmt.setString(1, filiacao.getNomePai());
      stmt.setString(2, filiacao.getNomeMae());
      stmt.setLong(3, dto.getId());
      stmt.executeUpdate();
    }
  }

  private void runPessoaFornecedorUpdate(PessoaDTO dto, Connection conn) throws SQLException {
    if (dto.getDadosFornecedor() == null) {
      return;
    }
    try (PreparedStatement stmt = conn.prepareStatement(PessoaPersisterUpdateConsts.H10_PESSOA_FORNECEDOR_UPDATE_SQL)) {
      PessoaFornecedorDTO fornecedor = dto.getDadosFornecedor();
      stmt.setString(1, fornecedor.getAgencia());
      stmt.setString(2, fornecedor.getConta());
      stmt.setLong(3, dto.getId());
      stmt.executeUpdate();
    }
  }
}
