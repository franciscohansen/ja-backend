package br.com.ja.persistence.sync.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

@Component("h10-provider")
public class H10DataSourceProvider implements IDataSourceProvider{

  @Value("${habil10.datasource.url}")
  private String url;
  @Value("${habil10.datasource.username}")
  private String username;
  @Value("${habil10.datasource.password}")
  private String password;

  private DriverManagerDataSource dataSource;

  @Override
  public DriverManagerDataSource dataSource() {
    if (this.dataSource == null) {
      this.dataSource = new DriverManagerDataSource(this.url, this.username, this.password);
    }
    return this.dataSource;
  }
}
