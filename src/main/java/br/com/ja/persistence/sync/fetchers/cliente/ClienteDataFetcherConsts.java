package br.com.ja.persistence.sync.fetchers.cliente;

public class ClienteDataFetcherConsts {

  private ClienteDataFetcherConsts() {

  }

  public static final String H10_CLIENTE_SQL = "select p.id as id,\n" +
      "       1 as id_empresa,\n" +
      "       p.tipopessoa = 'JURIDICA' as pessoajuridica,\n" +
      "       p.nome as nome,\n" +
      "       p.razaosocial as razaosocial,\n" +
      "       p.cpfcnpj as cpfcnpj,\n" +
      "       p.rgie as rgie,\n" +
      "       p.datanascimentofundacao as datanascimento,\n" +
      "       p.email as email,\n" +
      "       p.estadocivil as estadocivil,\n" +
      "       pc.naturalde as natural,\n" +
      "       p.endlogradouro as endlogradouro,\n" +
      "       p.endcep as endcep,\n" +
      "       p.endnumero as endnumero,\n" +
      "       p.endbairro as endbairro,\n" +
      "       p.endmunicipio as endmunicipio,\n" +
      "       p.enduf as enduf,\n" +
      "       p.endcomplemento as endcomplemento,\n" +
      "       p.endpais as endpais,\n" +
      "       p.cobrlogradouro as cobrlogradouro,\n" +
      "       p.cobrcep as cobrcep,\n" +
      "       p.cobrmumero as cobrnumero,\n" +
      "       p.cobrbairro as cobrbairro,\n" +
      "       p.cobrmunicipio as cobrmunicipio,\n" +
      "       p.cobruf as cobruf,\n" +
      "       p.cobrcomplemento as cobrcomplemento,\n" +
      "       p.cobrpais as cobrpais,\n" +
      "       pf.nomepai as nomepai,\n" +
      "       pf.nomemae as nomemae,\n" +
      "       p.sexo as sexo\n" +
      "from pessoa p left join\n" +
      "    pessoa_cliente pc on p.id = pc.pessoa_id left join\n" +
      "    pessoacliente_filiacao pf on pc.pessoa_id = pf.pessoacliente_pessoa_id\n" +
      "order by p.id";

  public static final String H10_CLIENTES_FONES_SQL = "select\n" +
      "\tptr.idpessoa as id_pessoa,\n" +
      "\tpt.id as id_habil,\n" +
      "\tpt.deleted as deleted,\n" +
      "\t1 as id_empresa,\n" +
      "\tpt.ddd as ddd,\n" +
      "\tpt.numero as fone,\n" +
      "\tpt.observacoes as observacoes,\n" +
      "\tpt.padrao as padrao,\n" +
      "\tpt.ramal as ramal,\n" +
      "\tpt.tipo as tipo\n" +
      "from\n" +
      "\tpessoa_telefones pt\n" +
      "left join pessoa_telefones_ref ptr on\n" +
      "\t( pt.id = ptr.idtelefone )";

}
