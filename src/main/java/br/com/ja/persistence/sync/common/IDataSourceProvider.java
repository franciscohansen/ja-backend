package br.com.ja.persistence.sync.common;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

public interface IDataSourceProvider {
  DriverManagerDataSource dataSource();
}
