package br.com.ja.persistence.sync.fetchers.cliente;

import br.com.ja.model.Endereco;
import br.com.ja.model.cliente.Cliente;
import br.com.ja.model.cliente.ClienteTelefones;
import br.com.ja.model.enums.EEstadoCivil;
import br.com.ja.model.enums.ESexo;
import br.com.ja.persistence.sync.common.IDataSourceProvider;
import br.com.ja.util.FormatUtils;
import br.com.ja.util.ResultSetHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static br.com.ja.model.enums.EEstadoCivil.NAO_SE_APLICA;
import static br.com.ja.persistence.sync.fetchers.cliente.ClienteDataFetcherConsts.H10_CLIENTES_FONES_SQL;
import static br.com.ja.persistence.sync.fetchers.cliente.ClienteDataFetcherConsts.H10_CLIENTE_SQL;

@Component
public class ClienteDataFetcher {

  private static final Logger LOGGER = LoggerFactory.getLogger(ClienteDataFetcher.class);

  private final IDataSourceProvider provider;

  @Autowired
  public ClienteDataFetcher(@Qualifier("h10-provider") IDataSourceProvider provider) {
    this.provider = provider;
  }


  public List<Cliente> fetchClientes() {
    try (
        Connection conn = this.provider.dataSource().getConnection();
        PreparedStatement stmt = conn.prepareStatement(H10_CLIENTE_SQL);
        ResultSet rs = stmt.executeQuery()
    ) {
      return convertResultSet(rs);
    } catch (SQLException e) {
      LOGGER.error("Error fetching clientes", e);
    }
    return new ArrayList<>();
  }

  public List<Cliente> convertResultSet(ResultSet rs) throws SQLException {
    List<Cliente> clientes = new ArrayList<>();
    while (rs.next()) {
      ResultSetHelper helper = new ResultSetHelper(rs);
      Cliente cliente = new Cliente();
      cliente.setIdEmpresa(helper.getLong("id_empresa", 1L));
      cliente.setIdHabil(helper.getLong("id", null));
      cliente.setPessoaJuridica(helper.getBoolean("pessoajuridica"));
      cliente.setNome(helper.getString("nome"));
      cliente.setRazaoSocial(helper.getString("razaosocial"));
      cliente.setCpfCnpj(FormatUtils.getFormattedDoc(helper.getString("cpfcnpj")));
      cliente.setRgIe(helper.getString("rgie"));
      cliente.setDataNascimento(helper.getDate("datanascimento"));
      cliente.setEmail(helper.getString("email"));
      cliente.setEstadoCivil(EEstadoCivil.valueOf(helper.getString("estadocivil", NAO_SE_APLICA.name())));
      cliente.setNaturalidade(helper.getString("naturalidade"));
      cliente.setEndereco(
          Endereco.builder()
              .logradouro(helper.getString("endlogradouro"))
              .cep(helper.getString("endcep"))
              .numero(helper.getString("endnumero"))
              .bairro(helper.getString("endbairro"))
              .municipio(helper.getString("endmunicipio"))
              .uf(helper.getString("enduf"))
              .complemento(helper.getString("endcomplemento"))
              .pais(helper.getString("endpais"))
              .build());
      cliente.setEnderecoCarne(
          Endereco.builder()
              .logradouro(helper.getString("cobrlogradouro"))
              .cep(helper.getString("cobrcep"))
              .numero(helper.getString("cobrnumero"))
              .bairro(helper.getString("cobrbairro"))
              .municipio(helper.getString("cobrmunicipio"))
              .uf(helper.getString("cobruf"))
              .complemento(helper.getString("cobrcomplemento"))
              .pais(helper.getString("cobrpais"))
              .build());
      cliente.setNomeMae(helper.getString("nomemae"));
      cliente.setNomePai(helper.getString("nomepai"));
      cliente.setSexo(ESexo.valueOf(helper.getString("sexo", "MASCULINO")));
      cliente.setHasNewData(false);
      clientes.add(cliente);
    }
    return clientes;
  }

  public List<ClienteTelefones> fetchClientesFones() {
    try (
        Connection conn = this.provider.dataSource().getConnection();
        PreparedStatement stmt = conn.prepareStatement(H10_CLIENTES_FONES_SQL);
        ResultSet rs = stmt.executeQuery()
    ) {
      return convertFoneResultSet(rs);
    } catch (SQLException e) {
      LOGGER.error("Error fetching clientes", e);
    }
    return new ArrayList<>();
  }

  public List<ClienteTelefones> convertFoneResultSet(ResultSet rs) throws SQLException {
    List<ClienteTelefones> fones = new ArrayList<>();
    while (rs.next()) {
      ResultSetHelper helper = new ResultSetHelper(rs);
      Cliente c = new Cliente();
      c.setId(helper.getLong("id_pessoa", 1L));
      ClienteTelefones fone = new ClienteTelefones();
      fone.setDeleted(helper.getBoolean("deleted", false));
      fone.setIdEmpresa(helper.getLong("id_empresa", 1L));
      fone.setIdHabil(helper.getLong("id_habil", 0L));
      fone.setCliente(c);
      fone.setDdd(helper.getString("ddd", null));
      fone.setFone(helper.getString("fone", null));
      fone.setRamal(helper.getString("ramal", null));
      fone.setObservacoes(helper.getString("observacoes", null));
      fone.setPadrao(helper.getBoolean("padrao", false));
      fone.setTipo(helper.getString("tipo", null));
      fones.add(fone);
    }
    return fones;
  }


}
