package br.com.ja.persistence.sync.persisters.h10.pessoa;

public class PessoaPersisterInsertConsts {


  public static final String H10_PESSOA_CLIENTE_VEICULO_INSERT_SQL = "insert into pessoacliente_veiculos (deleted,ano,cor,descricao,marca,placa,renavam) values (false,?,?,?,?,?,?) returning id;";
  public static final String H10_PESSOA_CLIENTE_VEICULO_REF_INSERT_SQL = "insert into pessoacliente_veiculos_ref (idcliente, idveiculo) values (?,?);";
}
