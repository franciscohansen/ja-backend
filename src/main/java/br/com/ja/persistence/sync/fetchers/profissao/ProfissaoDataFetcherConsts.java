package br.com.ja.persistence.sync.fetchers.profissao;

public class ProfissaoDataFetcherConsts {
  public static final String H10_PROFISSAO_SQL = "select id as id,\n" +
      " 1 as id_empresa,\n" +
      " false as deleted,\n" +
      " descricao as descricao \n" +
      " from profissoes";
}
