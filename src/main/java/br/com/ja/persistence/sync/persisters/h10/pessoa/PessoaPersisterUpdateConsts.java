package br.com.ja.persistence.sync.persisters.h10.pessoa;

public class PessoaPersisterUpdateConsts {

  public static final String H10_PESSOA_UPDATE_SQL = "update " +
      "pessoa " +
      "set " +
      "nome =?," +
      "razaosocial =?," +
      "tipopessoa =?," +
      "cpfcnpj =?," +
      "rgie =?," +
      "email =?," +
      "endbairro = ?," +
      "endcep =?," +
      "endcomplemento =?," +
      "endlogradouro =?," +
      "endmunicipio =?," +
      "endnumero =?," +
      "endpais =?," +
      "enduf =?," +
      "entrbairro =?," +
      "entrcep =?," +
      "entrcomplemento =?," +
      "entrlogradouro =?," +
      "entrmunicipio =?," +
      "entrnumero =?" +
      "entrpais =?," +
      "entruf =?," +
      "sexo =?," +
      "estadocivil =?," +
      "cliente =?," +
      "fornecedor =?," +
      "datanascimentofundacao =?" +
      "where" +
      "id =?;";

  public static final String H10_PESSOA_CLIENTE_UPDATE_SQL = "update " +
      "pessoa_cliente " +
      "set " +
      "naturalde =?" +
      "where" +
      "pessoa_id =?;";

  public static final String H10_PESSOA_CLIENTE_FILIACAO_UPDATE_SQL = "update " +
      "pessoacliente_filiacao " +
      "set " +
      "nomemae =?," +
      "nomepai =?" +
      "where" +
      "pessoacliente_pessoa_id =?;";

  public static final String H10_PESSOA_FORNECEDOR_UPDATE_SQL = "update " +
      "pessoafornecedor " +
      "set " +
      "agencia =?," +
      "conta =?" +
      "where" +
      "pessoa_id =?;";

  public static final String H10_PESSOA_CLIENTE_VEICULO_UPDATE_SQL = "update " +
      "pessoacliente_veiculos " +
      "set " +
      "marca = ?," +
      "descricao = ?," +
      "placa = ?," +
      "renavam = ?," +
      "ano = ?," +
      "cor = ?" +
      "where" +
      "(" +
      "select" +
      "pr.idcliente" +
      "from" +
      "pessoacliente_veiculos_ref pr" +
      "where" +
      "pr.idveiculo = id) = ?";




}
