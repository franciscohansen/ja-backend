package br.com.ja.persistence.sync.fetchers.profissao;

import br.com.ja.model.Profissao;
import br.com.ja.util.ResultSetHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import br.com.ja.persistence.sync.common.IDataSourceProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static br.com.ja.persistence.sync.fetchers.cliente.ClienteDataFetcherConsts.H10_CLIENTE_SQL;

@Component
public class ProfissaoDataFetcher {

  private static final Logger LOGGER = LoggerFactory.getLogger(ProfissaoDataFetcher.class);
  private final IDataSourceProvider provider;

  @Autowired
  public ProfissaoDataFetcher(@Qualifier("h10-provider") IDataSourceProvider provider) {
    this.provider = provider;
  }

  public List<Profissao> fetchProfissoes() {
    try (Connection conn = this.provider.dataSource().getConnection();
         PreparedStatement stmt = conn.prepareStatement(H10_CLIENTE_SQL);
         ResultSet rs = stmt.executeQuery()) {
      return convertResultSet(rs);
    } catch (SQLException e) {
      LOGGER.error("Error fetching Profissoes", e);
      return new ArrayList<>();
    }
  }

  private List<Profissao> convertResultSet(ResultSet rs) throws SQLException {
    List<Profissao> profissoes = new ArrayList<>();
    while (rs.next()) {
      ResultSetHelper helper = new ResultSetHelper(rs);
      Profissao p = new Profissao();
      String descricao = helper.getString("descricao", "");
      if (descricao.isEmpty()) {
        continue;
      }
      p.setDescricao(descricao);
      p.setIdHabil(helper.getLong("id", null));
      p.setIdEmpresa(helper.getLong("id_empresa", 1L));
      p.setDeleted(helper.getBoolean("deleted", false));
      profissoes.add(p);
    }
    return profissoes;
  }
}
