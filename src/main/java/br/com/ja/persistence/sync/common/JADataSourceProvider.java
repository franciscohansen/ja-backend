package br.com.ja.persistence.sync.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

@Component("ja-provider")
public class JADataSourceProvider implements IDataSourceProvider{

  @Value("${spring.datasource.url}")
  private String url;
  @Value("${spring.datasource.username}")
  private String username;
  @Value("${spring.datasource.password}")
  private String password;

  private DriverManagerDataSource dataSource;

  @Override
  public DriverManagerDataSource dataSource() {
    if (this.dataSource == null) {
      this.dataSource = new DriverManagerDataSource(this.url, this.username, this.password);
    }
    return this.dataSource;
  }
}
