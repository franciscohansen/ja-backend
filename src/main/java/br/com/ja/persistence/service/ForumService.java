package br.com.ja.persistence.service;

import br.com.ja.model.Forum;
import br.com.ja.persistence.repositories.ForumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ForumService extends AbstractService<Forum, ForumRepository> {
  private ForumRepository repository;

  @Autowired
  private UsuarioService usuarioService;

  @Override
  public String token() {
    return this.usuarioService.token();
  }
  @Override
  public ForumRepository getRepository() {
    return repository;
  }
}
