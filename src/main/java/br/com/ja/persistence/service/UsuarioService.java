package br.com.ja.persistence.service;

import br.com.ja.model.Usuario;
import br.com.ja.persistence.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsuarioService extends AbstractService<Usuario, UsuarioRepository> {

  @Autowired
  private UsuarioRepository repository;

  @Override
  public UsuarioRepository getRepository() {
    return repository;
  }

  @Override
  public String token() {
    List<Usuario> usuarios = this.repository.findAll();
    String sToken = null;
    for (Usuario u : usuarios) {
      sToken = u.getToken();
      if (u.isAdministrador() && !sToken.isEmpty()) {
        break;
      }
    }
    return sToken;
  }
}
