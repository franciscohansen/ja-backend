package br.com.ja.persistence.service;

import br.com.ja.model.Configuracao;
import br.com.ja.persistence.repositories.ConfiguracaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ConfiguracaoService extends AbstractService<Configuracao, ConfiguracaoRepository> {
  @Autowired
  private ConfiguracaoRepository repository;
  @Value("${habil10.host}")
  private String api;

  @Override
  public String token() {
    return null;
  }

  @Override
  public ConfiguracaoRepository getRepository() {
    return repository;
  }

  public String api() {
    return this.api;
  }


}
