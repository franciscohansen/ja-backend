package br.com.ja.persistence.service;

import br.com.ja.dto.AbstractDTO;
import br.com.ja.dto.conta.ContaDTO;
import br.com.ja.dto.conta.ContaDetalheDTO;
import br.com.ja.dto.item.ItemDTO;
import br.com.ja.dto.item.ItemDetalheDTO;
import br.com.ja.dto.item.ItemDetalhePrecosDTO;
import br.com.ja.dto.pessoa.PessoaDTO;
import br.com.ja.dto.venda.*;
import br.com.ja.model.Configuracao;
import br.com.ja.model.Endereco;
import br.com.ja.model.Funcionario;
import br.com.ja.model.cliente.Cliente;
import br.com.ja.model.cliente.ClienteTelefones;
import br.com.ja.model.cliente.ClienteVeiculo;
import br.com.ja.model.contatos.*;
import br.com.ja.model.contatos.dadosadicionais.DadosAdicionais;
import br.com.ja.model.contatos.dadosadicionais.Veiculo;
import br.com.ja.model.contatos.enums.EStatusContato;
import br.com.ja.model.contatos.enums.ETipoContrato;
import br.com.ja.model.contatos.enums.ETipoParcela;
import br.com.ja.model.juridico.EJuridicoSituacao;
import br.com.ja.model.juridico.Juridico;
import br.com.ja.model.juridico.situacao.JuridicoSituacao;
import br.com.ja.persistence.repositories.ConfiguracaoRepository;
import br.com.ja.persistence.repositories.ContatoSimulacaoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static br.com.ja.util.Constants.*;

@Service
public class ContatoSimulacaoService extends AbstractService<ContatoSimulacao, ContatoSimulacaoRepository> {

  private static final Logger LOGGER = LoggerFactory.getLogger(ContatoSimulacaoService.class);
  public static final String X_AUTH_TOKEN = "X-Auth-Token";
  private static final String BASE_URL = "http://%s:%d/movimentacao/%s";

  public static final String RECEBIMENTO = "RECEBIMENTO";
  public static final String PAGAMENTO = "PAGAMENTO";
  public static final String CREDIARIO = "CREDIARIO";

  private final ContatoSimulacaoRepository repository;
  private final ObjectMapper om;
  private final ClienteService clienteService;
  private final ConfiguracaoRepository cfgRepo;
  private final UsuarioService usuarioService;
  private final JuridicoService juridicoService;
  private ReportService reportService;

  private final RestTemplate rt = new RestTemplate();

  @Autowired
  public ContatoSimulacaoService(ContatoSimulacaoRepository repository, ObjectMapper om, ClienteService clienteService, ConfiguracaoRepository cfgRepo, UsuarioService usuarioService, JuridicoService juridicoService, ReportService reportService) {
    this.repository = repository;
    this.om = om;
    this.clienteService = clienteService;
    this.cfgRepo = cfgRepo;
    this.usuarioService = usuarioService;
    this.juridicoService = juridicoService;
    this.reportService = reportService;
  }


  @Transactional
  public void saveCliente(ContatoSimulacao obj) {
    if (obj.getDadosCliente() != null && (obj.getDadosCliente().getNovo() || obj.getDadosCliente().getCliente() == null)) {
      DadosCliente dc = obj.getDadosCliente();
      Cliente cliente = Cliente.builder()
          .nome(dc.getNome())
          .cpfCnpj(dc.getCpfCnpj())
          .dataNascimento(dc.getDataNascimento())
          .email(dc.getEmail())
          .endereco(dc.getEndereco())
          .enderecoCarne(dc.getEnderecoCarne())
          .estadoCivil(dc.getEstadoCivil())
          .naturalidade(dc.getNaturalidade())
          .nomeMae(dc.getNomeMae())
          .nomePai(dc.getNomePai())
          .pessoaJuridica(dc.getPessoaJuridica())
          .profissao(dc.getProfissao())
          .razaoSocial(dc.getRazaoSocial())
          .rgIe(dc.getRgIe())
          .sexo(dc.getSexo())
          .build();
      List<ClienteTelefones> fones = new ArrayList<>();
      for (DadosClienteTelefone f : dc.getTelefones()) {
        fones.add(
            ClienteTelefones.builder()
                .ddd(f.getDdd())
                .fone(f.getFone())
                .observacoes(f.getObservacoes())
                .padrao(f.getPadrao())
                .ramal(f.getRamal())
                .tipo(f.getTipo())
                .build()
        );
      }
      cliente.setTelefones(fones);
      cliente = this.clienteService.save(cliente);
      dc.setCliente(cliente);
    }
  }

  @Override
  public String token() {
    return this.usuarioService.token();
  }

  @Override
  public ContatoSimulacaoRepository getRepository() {
    return repository;
  }

  private String getToken() {
    return this.usuarioService.token();
  }

  @Transactional
  public Cliente achaOuCadastraIndicador(ContatoSimulacao contato) {
    Indicacao ind = contato.getDadosIndicacao();
    Cliente c = ind.getCliente();
    if (c != null && c.getIdHabil() > 0) {
      return c;
    }
    Cliente cliente = Cliente.builder()
        .nome(ind.getNome())
        .cpfCnpj(ind.getCpfCnpj())
        .rgIe(ind.getRgIe())
        .banco(ind.getBanco())
        .agencia(ind.getAgencia())
        .contaBancaria(ind.getContaBancaria())
        .build();
    return this.clienteService.save(cliente);
  }

  @Transactional
  public Cliente achaOuCadastraCliente(ContatoSimulacao contato) {
    DadosCliente dados = contato.getDadosCliente();
    Cliente c = dados.getCliente();
    Cliente cliente = Cliente.builder()
        .pessoaJuridica(dados.getPessoaJuridica())
        .nome(dados.getNome())
        .razaoSocial(dados.getRazaoSocial())
        .cpfCnpj(dados.getCpfCnpj())
        .rgIe(dados.getRgIe())
        .dataNascimento(dados.getDataNascimento())
        .email(dados.getEmail())
        .estadoCivil(dados.getEstadoCivil())
        .naturalidade(dados.getNaturalidade())
        .endereco(dados.getEndereco())
        .enderecoCarne(dados.getEnderecoCarne())
        .nomePai(dados.getNomePai())
        .nomeMae(dados.getNomeMae())
        .sexo(dados.getSexo())
        .profissao(dados.getProfissao())
        .build();
    List<ClienteTelefones> fones = new ArrayList<>();
    for (DadosClienteTelefone fone : dados.getTelefones()) {
      fones.add(
          ClienteTelefones.builder()
              .padrao(fone.getPadrao())
              .tipo(fone.getTipo())
              .ddd(fone.getDdd())
              .fone(fone.getFone())
              .ramal(fone.getRamal())
              .observacoes(fone.getObservacoes())
              .build()
      );
    }
    if (c != null && c.getIdHabil() > 0) {
      cliente.setId(c.getId());
      cliente.setIdHabil(c.getIdHabil());
    }
    cliente.setTelefones(fones);
    cliente.setVeiculos(buildVeiculos(c, contato));
    cliente.setHasNewData(true);
    return this.clienteService.save(cliente);
  }

  private List<ClienteVeiculo> buildVeiculos(Cliente cliente, ContatoSimulacao contato) {
    List<ClienteVeiculo> veiculos = new ArrayList<>();
    if (cliente != null) {
      veiculos = cliente.getVeiculos();
    }
    if (Optional.ofNullable(contato.getDadosAdicionais()).orElse(new DadosAdicionais()).getVeiculo() != null) {
      Veiculo v = contato.getDadosAdicionais().getVeiculo();
      veiculos.add(
          ClienteVeiculo.builder()
              .ano(v.getAno())
              .anoModelo(v.getAnoModelo())
              .chassi(v.getChassi())
              .cor(v.getCor())
              .cnh(v.getCnh())
              .cpfCondutor(v.getCpfCondutor())
              .crlvAno(v.getCrlvAno())
              .renavam(v.getRenavam())
              .marca(v.getMarca())
              .modelo(v.getModelo())
              .nomeCondutor(v.getNomeCondutor())
              .placa(v.getPlaca())
              .build()
      );
    }
    return veiculos;
  }


  private String baseUrl(String host, int port) {
    return "http://" + host + ":" + port + "/movimentacao/venda";
  }

  private Long getReferencia(Configuracao cfg) {
    String url = baseUrl(cfg.getHostH10(), cfg.getPortH10()) + "/gera-referencia";
    try {
      RequestEntity<Void> request = RequestEntity.get(new URI(url))
          .header(X_AUTH_TOKEN, getToken())
          .build();
      ResponseEntity<Long> response = rt.exchange(request, Long.class);
      if (response.getStatusCode().equals(HttpStatus.OK)) {
        return response.getBody();
      } else {
        return 0L;
      }
    } catch (URISyntaxException e) {
      e.printStackTrace();
      return 0L;
    }
  }

  @Transactional
  public ContatoSimulacao geraVenda(Long id, boolean somenteParcelas) {
    ContatoSimulacao contato = this.repository.getById(id);
    if (contato.getDadosCliente().getCliente() == null || contato.getDadosCliente().getCliente().getIdHabil() <= 0) {
      //Não existe no Hábil 10
      contato.getDadosCliente().setCliente(achaOuCadastraCliente(contato));
    }
    List<Configuracao> list = cfgRepo.findAll();
    if (!list.isEmpty()) {
      Configuracao cfg = list.get(0);
      contato = geraVendaParcelas(contato, cfg);
      if (!somenteParcelas) {
        contato = geraVendaAdesao(contato, cfg);
      }
    }
    return repository.save(contato);
  }

  @Transactional
  public ContatoSimulacao geraVendaParcelas(ContatoSimulacao contato, Configuracao cfg) {
    try {
      Long referencia = getReferencia(cfg);

      VendaDTO venda = buildVendaParcelas(contato, referencia);
      List<ContaDTO> contas = buildContasParcela(contato, referencia);
      contas.add(buildContaQuitacao(contato, getReferencia(cfg), cfg));
      ContaDTO comissao = buildContaComissao(contato, cfg);
      if (comissao != null) {
        contas.add(comissao);
      }
      enviaVenda(contato, cfg, venda, contas, false);
    } catch (IOException | URISyntaxException e) {
      e.printStackTrace();
    }
    return repository.save(contato);
  }

  private VendaDTO buildVendaParcelas(ContatoSimulacao contato, Long referencia) {
    VendaDTO venda = baseVenda(contato, referencia);
    venda.setTotais(VendaTotaisDTO.builder()
        .total(contato.getSimulacao().getRenSaldo())
        .totalProdutos(contato.getSimulacao().getRenSaldo())
        .build()
    );
    Funcionario vendedor = contato.getVendedor();
    ItemDTO item = ItemDTO.builder()
        .detalhes(Collections.singletonList(
            ItemDetalheDTO.builder()
                .empresa(new AbstractDTO(1L))
                .precos(
                    ItemDetalhePrecosDTO.builder()
                        .precoVenda(contato.getAdesao().getValor())
                        .precoCusto(contato.getAdesao().getValor())
                        .build()
                )
                .build()
        ))
        .build();
    item.setId(2L);
    venda.setDetalhes(Collections.singletonList(
        VendaDetalheDTO.builder()
            .item(item)
            .qtde(1D)
            .descricaoProduto("ACORDO EXTRAJUDICIAL")
            .preco(contato.getSimulacao().getRenSaldo())
            .total(contato.getSimulacao().getRenSaldo())
            .vendedor(vendedor != null ? new AbstractDTO(vendedor.getIdHabil()) : null)
            .itemRefDetalhe(VendaDetalheRefDetalheDTO.builder()
                .tipo("DETALHE")
                .idDetalhe(2L)
                .build()
            )
            .build()
    ));
    venda.setCentroCusto(new AbstractDTO(2L));
    return venda;
  }

  private ContaDTO buildConta(PessoaDTO pessoa, Funcionario vendedor, String descricao, Long referencia,
                              String tipo, double valor, Long contaCaixa, Long centroCusto, Date cadastro, Date vencimento, String documento) {
    return ContaDTO.builder()
        .empresa(new AbstractDTO(1L))
        .pessoa(pessoa)
        .vendedor(vendedor != null ? new AbstractDTO(vendedor.getIdHabil()) : null)
        .descricao(Optional.ofNullable(descricao).orElse("Conta a Receber"))
        .referencia(referencia)
        .pedeDados(CREDIARIO)
        .tipo(tipo)
        .cadastro(cadastro)
        .vencimento(vencimento)
        .documento(documento)
        .cancelado(false)
        .detalhes(
            Collections.singletonList(
                ContaDetalheDTO.builder()
                    .valor(valor)
                    .contaCaixa(new AbstractDTO(contaCaixa))
                    .centroCusto(new AbstractDTO(centroCusto))
                    .build()
            )
        )
        .build();
  }

  private PessoaDTO buildPessoa(ContatoSimulacao contato) {
    PessoaDTO pessoa = new PessoaDTO();
    if (contato.getDadosCliente().getCliente() != null) {
      pessoa.setId(contato.getDadosCliente().getCliente().getIdHabil());
    }
    return pessoa;
  }

  private ContaDTO buildContaQuitacao(ContatoSimulacao contato, Long referencia, Configuracao cfg) {
    String descricao = "Previsão de Quitação";
    if (ETipoContrato.VEICULO.equals(contato.getTipoContrato()) && contato.getDadosAdicionais().getVeiculo() != null) {
      Veiculo veiculo = contato.getDadosAdicionais().getVeiculo();
      descricao += ": " + veiculo.getModelo() + " - " + veiculo.getPlaca();
    }
    Funcionario vendedor = contato.getVendedor();
    PessoaDTO pessoa = buildPessoa(contato);
    double valor = contato.getSimulacao().getSaldoPagar();
    valor -= (valor * (cfg.getPercentualQuitacao() / 100));

    return buildConta(pessoa, vendedor, descricao, referencia, PAGAMENTO, valor, 182L, 3L, contato.getDataLcto(), contato.getSimulacao().getPrevisaoQuitacao(), contato.getId().toString());
  }

  private List<ContaDTO> buildContasParcela(ContatoSimulacao contato, Long referencia) {
    List<SimulacaoParcelas> parcelas = contato.getParcelas()
        .stream()
        .filter(p -> p.getTipo().equals(ETipoParcela.PARCELA))
        .collect(Collectors.toList());
    List<ContaDTO> contas = new ArrayList<>();
    for (int i = 0; i < parcelas.size(); i++) {
      SimulacaoParcelas parcela = parcelas.get(i);
      String descricao = "Acordo ";
      descricao = getDescricao(contato, i, parcela, descricao);
      Funcionario vendedor = contato.getVendedor();
      PessoaDTO pessoa = new PessoaDTO();
      if (contato.getDadosCliente().getCliente() != null) {
        pessoa.setId(contato.getDadosCliente().getCliente().getIdHabil());
      }
      contas.add(
          buildConta(pessoa, vendedor, descricao, referencia, RECEBIMENTO, parcela.getValor(), 179L, 1L, Calendar.getInstance().getTime(), parcela.getVencimento(), parcela.getDocumento())
      );
    }
    return contas;
  }

  private ContaDTO buildContaComissao(ContatoSimulacao contato, Configuracao cfg) {
    if (contato.getDadosIndicacao() != null && Optional.ofNullable(contato.getComissao()).orElse(0D) > 0D) {
      List<SimulacaoParcelas> parcelas = contato.getParcelas()
          .stream()
          .filter(p -> ETipoParcela.ADESAO.equals(p.getTipo()))
          .sorted(Comparator.comparing(SimulacaoParcelas::getDocumento))
          .collect(Collectors.toList());

      SimulacaoParcelas parcela = parcelas.get(parcelas.size() - 1);
      Date vencimento = parcela.getVencimento();

      Indicacao indicacao = contato.getDadosIndicacao();
      Long referencia = getReferencia(cfg);
      Cliente c = achaOuCadastraIndicador(contato);
      String descricao = "Indicação: " + c.getNome();
      PessoaDTO pessoa = new PessoaDTO();
      if (c != null) {
        pessoa.setId(c.getIdHabil());
      }
      return buildConta(pessoa, contato.getVendedor(), descricao, referencia, PAGAMENTO, contato.getComissao(),
          192L, 10L, Calendar.getInstance().getTime(), vencimento, contato.getId().toString());
    } else {
      return null;
    }
  }


  @Transactional
  public ContatoSimulacao geraVendaAdesao(ContatoSimulacao contato, Configuracao cfg) {
    try {
      Long referencia = getReferencia(cfg);

      VendaDTO venda = buildVendaAdesao(contato, referencia);
      List<ContaDTO> contas = buildContasAdesao(contato, referencia);

      enviaVenda(contato, cfg, venda, contas, true);
    } catch (IOException | URISyntaxException e) {
      e.printStackTrace();
    }
    return repository.save(contato);
  }

  private VendaDTO baseVenda(ContatoSimulacao contato, Long referencia) {
    DadosCliente cliente = contato.getDadosCliente();
    Endereco endereco = Optional.ofNullable(cliente.getEndereco()).orElse(new Endereco());
    endereco.setCep(Optional.ofNullable(endereco.getCep()).orElse("").replace("-", ""));
    Funcionario vendedor = contato.getVendedor();
    CfopDTO cfop = new CfopDTO("5102");
    cfop.setId(282L);
    Cliente c = achaOuCadastraCliente(contato);
    if (cliente.getCliente() == null) {
      cliente.setCliente(c);
    }
    Long idPessoa = c.getIdHabil();
    return VendaDTO.builder()
        .tipo("VENDA")
        .subTipo("VENDA")
        .cfop(cfop)
        .pessoa(new AbstractDTO(idPessoa))
        .vendedor(vendedor != null ? new AbstractDTO(vendedor.getIdHabil()) : null)
        .empresa(new AbstractDTO(1L))
        .dataEmissao(Calendar.getInstance().getTime())
        .dataCadastro(Calendar.getInstance().getTime())
        .referencia(referencia)
        .infoPessoa(
            VendaInfoPessoaDTO.builder()
                .nomePessoa(cliente.getNome())
                .cpfCnpj(cliente.getCpfCnpj())
                .rgIe(cliente.getRgIe())
                .endereco(endereco)
                .build()
        )
        .infoNFe(VendaInfoNFeDTO.builder().nroNota(contato.getId()).build())
        .observacoes(contato.getDadosAdicionais().getObservacoes())
        .build();
  }


  private VendaDTO buildVendaAdesao(ContatoSimulacao contato, Long referencia) {

    VendaDTO venda = baseVenda(contato, referencia);

    venda.setTotais(VendaTotaisDTO.builder()
        .total(contato.getAdesao().getValor())
        .totalProdutos(contato.getAdesao().getValor())
        .build()
    );
    Funcionario vendedor = contato.getVendedor();
    ItemDTO item = ItemDTO.builder()
        .detalhes(Collections.singletonList(
            ItemDetalheDTO.builder()
                .empresa(new AbstractDTO(1L))
                .precos(
                    ItemDetalhePrecosDTO.builder()
                        .precoVenda(contato.getAdesao().getValor())
                        .precoCusto(contato.getAdesao().getValor())
                        .build()
                )
                .build()
        ))
        .build();
    item.setId(1L);
    venda.setDetalhes(Collections.singletonList(
        VendaDetalheDTO.builder()
            .item(item)
            .qtde(1D)
            .descricaoProduto("ADESÃO DE SERVIÇOS - I")
            .preco(contato.getAdesao().getValor())
            .total(contato.getAdesao().getValor())
            .vendedor(vendedor != null ? new AbstractDTO(vendedor.getIdHabil()) : null)
            .itemRefDetalhe(
                VendaDetalheRefDetalheDTO.builder()
                    .idDetalhe(1L)
                    .tipo("DETALHE")
                    .build()
            )
            .build()
    ));
    venda.setCentroCusto(new AbstractDTO(1L));
    return venda;
  }

  private List<ContaDTO> buildContasAdesao(ContatoSimulacao contato, Long referencia) {
    List<SimulacaoParcelas> parcelas = contato.getParcelas()
        .stream()
        .filter(p -> p.getTipo().equals(ETipoParcela.ADESAO))
        .collect(Collectors.toList());
    List<ContaDTO> contas = new ArrayList<>();
    for (int i = 0; i < parcelas.size(); i++) {
      SimulacaoParcelas parcela = parcelas.get(i);
      String descricao = "Adesão ";
      descricao = getDescricao(contato, i, parcela, descricao);
      Funcionario vendedor = contato.getVendedor();
      contas.add(
          buildConta(buildPessoa(contato), vendedor, descricao, referencia, RECEBIMENTO, parcela.getValor(),
              178L, 2L, Calendar.getInstance().getTime(), parcela.getVencimento(), parcela.getDocumento())
      );
    }
    return contas;
  }

  private String getDescricao(ContatoSimulacao contato, int i, SimulacaoParcelas parcela, String descricao) {
    switch (contato.getTipoContrato()) {
      case EMPRESTIMOS: {
        descricao += " Empréstimo";
        break;
      }
      case VEICULO: {
        Veiculo veiculo = contato.getDadosAdicionais().getVeiculo();
        descricao += veiculo.getModelo() + " " +
            veiculo.getPlaca();
        break;
      }
      case CARTAO_CREDITO: {
        descricao += " Cartão de Crédito";
        break;
      }
      default:
        descricao += "";
    }
    descricao += " (" + parcela.getDocumento() + " - " + i + ")";
    return descricao;
  }

  private void enviaVenda(ContatoSimulacao contato, Configuracao cfg, VendaDTO venda, List<ContaDTO> contas, boolean isAdesao) throws IOException, URISyntaxException {
    ByteArrayOutputStream outVenda = new ByteArrayOutputStream();
    ByteArrayOutputStream outContas = new ByteArrayOutputStream();
    this.om.writeValue(outVenda, venda);
    this.om.writeValue(outContas, contas);

    String url = String.format(BASE_URL + "/faturar", cfg.getHostH10(), cfg.getPortH10(), "venda");

    HttpHeaders headers = new HttpHeaders();
    headers.add(X_AUTH_TOKEN, getToken());

    MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    String pVenda = new String(outVenda.toByteArray(), StandardCharsets.UTF_8);
    String pContas = new String(outContas.toByteArray(), StandardCharsets.UTF_8);
    params.add("dados", pVenda);
    params.add("documentos", pContas);
    LOGGER.warn(pVenda);
    LOGGER.warn(pContas);

    HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(params, headers);
    ResponseEntity<VendaDTO> response = this.rt.postForEntity(new URI(url), request, VendaDTO.class);
    if (response.getStatusCode().equals(HttpStatus.OK) && response.hasBody() && response.getBody() != null) {
      VendaDTO dto = response.getBody();
      if (dto != null) {
        if (isAdesao) {
          contato.setIdVendaAdesao(dto.getId());
          contato.setRefVendaAdesao(dto.getReferencia());
          contato.setDataVendaAdesao(venda.getDataEmissao());
        } else {
          contato.setIdVendaParcela(dto.getId());
          contato.setRefVendaParcela(dto.getReferencia());
          contato.setDataVendaParcela(venda.getDataEmissao());
        }
      }
      contato.setStatus(EStatusContato.VENDIDO);
    }
  }


  public Specification<ContatoSimulacao> juridicoFindAll(HttpServletRequest request, String search) {
    return (root, query, builder) -> {
      String sLike = "%" + Optional.ofNullable(search).orElse("").toLowerCase() + "%";
      Join<ContatoSimulacao, Juridico> juridico = root.join("juridico", JoinType.LEFT);
      Predicate where = builder.and(
          builder.isFalse(builder.coalesce(root.get("deleted"), false)),
          builder.isFalse(builder.coalesce(juridico.get("deleted"), false)),
          builder.equal(root.get("status"), EStatusContato.VENDIDO),
          builder.greaterThan(root.get("refVendaParcela"), 0),
          builder.isNotNull(root.get("juridico"))
      );
      String sUF = Optional.ofNullable(request.getHeader("uf")).orElse("");
      if (!sUF.isEmpty()) {
        where = builder.and(
            where,
            builder.equal(root.get(DADOS_CLIENTE).get("endereco").get("uf"), sUF)
        );
      }
      String sDataInicial = Optional.ofNullable(request.getHeader("data-inicial")).orElse("");
      String sDataFinal = Optional.ofNullable(request.getHeader("data-final")).orElse("");
      Date di = null;
      Date df = null;
      if (!sDataInicial.isEmpty() && !sDataFinal.isEmpty()) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        try {
          di = sdf.parse(sDataInicial);
          df = sdf.parse(sDataFinal);
        } catch (Exception ignored) {
          //DO NOTHING
        }
      }
      String situacao = Optional.ofNullable(request.getHeader(SITUACAO)).orElse("");
      if (!situacao.isEmpty()) {
        Join<Juridico, JuridicoSituacao> situacoes = juridico.join("situacoes", JoinType.LEFT);
        where = builder.and(
            where,
            builder.equal(situacoes.get(SITUACAO), EJuridicoSituacao.valueOf(situacao)),
            builder.isTrue(builder.coalesce(situacoes.get("ativo"), false))
        );
        if (di != null && df != null) {
          where = builder.and(
              where,
              builder.between(situacoes.get("data"), di, df)
          );
        }
      } else {
        if (di != null && df != null) {
          where = builder.and(
              where,
              builder.between(root.get("dataVendaParcela"), di, df)
          );
        }
      }
      where = builder.and(
          where,
          builder.or(
              builder.like(builder.function(UNACCENT_FUNCTION, String.class, builder.lower(root.get(DADOS_CLIENTE).get("nome"))), sLike),
              builder.like(builder.function(UNACCENT_FUNCTION, String.class, builder.lower(root.get(DADOS_CLIENTE).get("razaoSocial"))), sLike),
              builder.like(builder.function(UNACCENT_FUNCTION, String.class, builder.lower(root.get(DADOS_CLIENTE).get("cpfCnpj"))), sLike)
          )
      );
      query.orderBy(builder.desc(juridico.get("id")));
      return where;
    };
  }

  @Transactional
  public boolean inativar(Long id) {
    ContatoSimulacao cs = getRepository().getById(id);
    cs.setInativo(true);
    getRepository().save(cs);
    return true;
  }

  @Transactional
  public boolean criaJuridico(Long id) {
    Optional<ContatoSimulacao> cs = getRepository().findById(id);
    if (cs.isPresent()) {
      Juridico juridico = Juridico.builder()
          .contato(cs.get())
          .situacoes(new ArrayList<>())
          .visualizacoes(new ArrayList<>())
          .build();
      this.juridicoService.save(juridico);
      this.juridicoService.criaSituacoesFaltantesJuridico(juridico);
      return true;
    } else {
      return false;
    }
  }

  @Transactional
  public boolean cancelar(Long id) throws URISyntaxException {
    Optional<ContatoSimulacao> opt = getRepository().findById(id);
    if (!opt.isPresent()) {
      return false;
    }
    ContatoSimulacao cs = opt.get();
    cs.setCancelado(true);

    if (Optional.ofNullable(cs.getIdHabil()).orElse(0L) > 0) {
      return cancelaHabil(cs.getIdHabil());
    }
    return true;
  }


  private boolean cancelaHabil(Long idHabil) throws URISyntaxException {
    Configuracao cfg = this.cfgRepo.findFirstByOrderByIdDesc().orElseThrow(() -> new EntityNotFoundException("Configuracao not found"));
    String baseUrl = this.baseUrl(cfg.getHostH10(), cfg.getPortH10());
    String findUrl = baseUrl + "/" + idHabil;
    RequestEntity<Void> request = RequestEntity.get(new URI(findUrl))
        .header(X_AUTH_TOKEN, token())
        .build();
    ResponseEntity<String> response = this.rt.exchange(request, String.class);
    if (HttpStatus.OK.equals(response.getStatusCode())) {
      String cancelUrl = baseUrl + "/cancelar";
      HttpHeaders headers = new HttpHeaders();
      headers.add(X_AUTH_TOKEN, token());

      MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
      map.add("dados", "[" + response.getBody() + "]");

      HttpEntity<MultiValueMap<String, String>> cancelRequest = new HttpEntity<>(map, headers);
      ResponseEntity<String> cancelResponsee = this.rt.exchange(cancelUrl, HttpMethod.POST, cancelRequest, String.class);
      return HttpStatus.OK.equals(cancelResponsee.getStatusCode());
    }
    return true;
  }



  @Override
  protected String callPrintReport(Long id, Long reportId) {
    return this.reportService.callPrintReport(id, reportId);
  }
}
