package br.com.ja.persistence.service;

import br.com.ja.model.Funcionario;
import br.com.ja.persistence.repositories.FuncionarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FuncionarioService extends AbstractService<Funcionario, FuncionarioRepository> {
  @Autowired
  private FuncionarioRepository repository;

  @Autowired
  private UsuarioService usuarioService;

  @Override
  public String token() {
    return this.usuarioService.token();
  }
  @Override
  public FuncionarioRepository getRepository() {
    return repository;
  }
}
