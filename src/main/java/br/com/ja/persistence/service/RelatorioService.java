package br.com.ja.persistence.service;

import br.com.ja.model.Relatorio;
import br.com.ja.persistence.repositories.RelatorioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RelatorioService extends AbstractService<Relatorio, RelatorioRepository> {
  @Autowired
  private RelatorioRepository repository;

  @Autowired
  private UsuarioService usuarioService;

  @Override
  public String token() {
    return this.usuarioService.token();
  }

  @Override
  public RelatorioRepository getRepository() {
    return repository;
  }
}
