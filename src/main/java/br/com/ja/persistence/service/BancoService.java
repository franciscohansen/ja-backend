package br.com.ja.persistence.service;

import br.com.ja.model.Banco;
import br.com.ja.persistence.repositories.BancoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class BancoService extends AbstractService<Banco, BancoRepository> {

  @Autowired
  private BancoRepository repository;

  @Autowired
  private UsuarioService usuarioService;

  @Override
  public String token() {
    return this.usuarioService.token();
  }

  @Override
  public BancoRepository getRepository() {
    return this.repository;
  }
}
