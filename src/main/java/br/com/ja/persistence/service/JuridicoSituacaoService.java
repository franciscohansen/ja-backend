package br.com.ja.persistence.service;

import br.com.ja.model.juridico.situacao.JuridicoSituacao;
import br.com.ja.persistence.repositories.JuridicoSituacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JuridicoSituacaoService extends AbstractService<JuridicoSituacao, JuridicoSituacaoRepository> {

  @Autowired
  private JuridicoSituacaoRepository repository;
  @Autowired
  private UsuarioService usuarioService;

  @Override
  public String token() {
    return this.usuarioService.token();
  }
  @Override
  public JuridicoSituacaoRepository getRepository() {
    return repository;
  }
}
