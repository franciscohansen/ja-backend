package br.com.ja.persistence.service;

import br.com.ja.model.Report;
import br.com.ja.persistence.repositories.ReportRepository;
import br.com.ja.persistence.sync.common.IDataSourceProvider;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@Service
public class ReportService extends AbstractService<Report, ReportRepository> {

  private UsuarioService usuarioService;
  private ReportRepository repository;
  private IDataSourceProvider provider;

  @Autowired
  public ReportService(UsuarioService usuarioService, ReportRepository repository, @Qualifier("ja-provider") IDataSourceProvider provider) {
    this.usuarioService = usuarioService;
    this.repository = repository;
    this.provider = provider;
  }

  @Override
  public String token() {
    return this.usuarioService.token();
  }

  @Override
  public ReportRepository getRepository() {
    return repository;
  }


  public String print(Long reportId, Long id) throws JRException, SQLException {
    Report report = this.repository.findById(reportId).orElseThrow(() -> new EntityNotFoundException("Report with id " + reportId + " not found"));
    Map<String, Object> parameters = new HashMap<>();
    parameters.put("id", id);
    JasperReport jReport = JasperCompileManager.compileReport(JRXmlLoader.load(report.getPath()));
    JasperPrint print = JasperFillManager.fillReport(jReport, parameters, this.provider.dataSource().getConnection());
    if (print.getPages().isEmpty()) {
      return null;
    }
    return encodeAndConvert(print);
  }

  private String encodeAndConvert(JasperPrint print) throws JRException {
    byte[] printed = JasperExportManager.exportReportToPdf(print);
    if (printed != null) {
      byte[] encoded = Base64.getEncoder().encode(printed);
      return new String(encoded, StandardCharsets.UTF_8);
    }
    return null;
  }
}
