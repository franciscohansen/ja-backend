package br.com.ja.persistence.service;

import br.com.ja.model.Empresa;
import br.com.ja.persistence.repositories.EmpresaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmpresaService extends AbstractService<Empresa, EmpresaRepository> {

  @Autowired
  private EmpresaRepository repository;
  @Autowired
  private UsuarioService usuarioService;

  @Override
  public String token() {
    return this.usuarioService.token();
  }
  @Override
  public EmpresaRepository getRepository() {
    return repository;
  }
}
