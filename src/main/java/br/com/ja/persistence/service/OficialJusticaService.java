package br.com.ja.persistence.service;

import br.com.ja.model.OficialJustica;
import br.com.ja.persistence.repositories.OficialJusticaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OficialJusticaService extends AbstractService<OficialJustica, OficialJusticaRepository> {
  private OficialJusticaRepository repository;

  @Autowired
  private UsuarioService usuarioService;

  @Override
  public String token() {
    return this.usuarioService.token();
  }
  @Override
  public OficialJusticaRepository getRepository() {
    return repository;
  }
}
