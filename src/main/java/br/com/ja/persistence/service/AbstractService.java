package br.com.ja.persistence.service;

import br.com.ja.model.AbstractModel;
import br.com.ja.persistence.repositories.IRepository;
import br.com.ja.util.report.ReportPrinter;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Optional;

public abstract class AbstractService<T extends AbstractModel, U extends IRepository<T>> {

  @PersistenceContext
  private EntityManager em;

  @Autowired
  private ReportPrinter printer;
  public abstract String token();

  public abstract U getRepository();

  protected EntityManager em() {
    return this.em;
  }

  public void doBeforeSave(T obj) {
    //Implementação nos filhos
  }

  public void doAfterSave(T obj) {
    //Implementação nos filhos
  }

  @Transactional
  public T save(T obj) {
    if (Optional.ofNullable(obj.getId()).orElse(0L) == 0L) {
      obj.setId(null);
    }
    obj.corrigeRelacoes();
    doBeforeSave(obj);
    obj = getRepository().save(obj);
    doAfterSave(obj);
    return obj;
  }

  protected String callPrintReport(Long id, Long reportId){
    return null;
  }


  public String imprimir(Long id, Long reportId){
    return callPrintReport(id, reportId);
  }
}
