package br.com.ja.persistence.service;

import br.com.ja.model.Profissao;
import br.com.ja.persistence.repositories.ProfissaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfissaoService extends AbstractService<Profissao, ProfissaoRepository> {
  @Autowired
  private ProfissaoRepository repository;

  @Autowired
  private UsuarioService usuarioService;

  @Override
  public String token() {
    return this.usuarioService.token();
  }

  @Override
  public ProfissaoRepository getRepository() {
    return repository;
  }
}
