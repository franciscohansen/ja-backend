package br.com.ja.persistence.service;

import br.com.ja.model.Documento;
import br.com.ja.persistence.repositories.DocumentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DocumentoService extends AbstractService<Documento, DocumentoRepository> {

  @Autowired
  private DocumentoRepository repository;
  @Autowired
  private UsuarioService usuarioService;

  @Override
  public String token() {
    return this.usuarioService.token();
  }
  @Override
  public DocumentoRepository getRepository() {
    return repository;
  }
}
