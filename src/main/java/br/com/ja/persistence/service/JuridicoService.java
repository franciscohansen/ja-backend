package br.com.ja.persistence.service;

import br.com.ja.model.juridico.EJuridicoSituacao;
import br.com.ja.model.juridico.Juridico;
import br.com.ja.model.juridico.situacao.JuridicoSituacao;
import br.com.ja.persistence.repositories.JuridicoRepository;
import br.com.ja.persistence.repositories.JuridicoSituacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.logging.Logger;

@Service
public class JuridicoService extends AbstractService<Juridico, JuridicoRepository> {

  private static final Logger LOG = Logger.getLogger(JuridicoService.class.getSimpleName());

  @Autowired
  private JuridicoRepository repository;
  @Autowired
  private JuridicoSituacaoRepository sitRepo;


  @Override
  public JuridicoRepository getRepository() {
    return repository;
  }

  @Autowired
  private UsuarioService usuarioService;

  @Override
  public String token() {
    return this.usuarioService.token();
  }

  @Transactional
  public void criaSituacoesFaltantes() {
    List<Juridico> juridico = this.repository.findAll(Specification.where((root, query, builder) -> builder.isFalse(builder.coalesce(root.get("deleted"), false))));
    for (Juridico j : juridico) {
     this.criaSituacoesFaltantesJuridico(j);
    }
  }

  @Transactional
  public void criaSituacoesFaltantesJuridico( Juridico juridico ){
    List<JuridicoSituacao> situacoes = juridico.getSituacoes();
    for (EJuridicoSituacao sit : EJuridicoSituacao.values()) {
      if (situacoes.stream().noneMatch(s -> s.getSituacao().equals(sit))) {
        LOG.info(() -> String.format("%d - %s", juridico.getId(), sit.name()));
        JuridicoSituacao situacao = JuridicoSituacao.builder()
            .juridico(juridico)
            .ativo(false)
            .situacao(sit)
            .build();
        situacao.setIdEmpresa(juridico.getIdEmpresa());
        situacao.setDeleted(false);
        this.sitRepo.save(situacao);
      }
    }
  }

}
