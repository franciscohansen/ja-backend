package br.com.ja.persistence.service;

import br.com.caelum.stella.validation.CNPJValidator;
import br.com.caelum.stella.validation.CPFValidator;
import br.com.caelum.stella.validation.InvalidStateException;
import br.com.caelum.stella.validation.Validator;
import br.com.ja.dto.pessoa.PessoaDTO;
import br.com.ja.exceptions.DocumentConflictException;
import br.com.ja.model.Configuracao;
import br.com.ja.model.cliente.Cliente;
import br.com.ja.persistence.repositories.ClienteRepository;
import br.com.ja.persistence.repositories.ConfiguracaoRepository;
import br.com.ja.util.FormatUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.persistence.EntityNotFoundException;
import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ClienteService extends AbstractService<Cliente, ClienteRepository> {

  private static final Logger LOGGER = LoggerFactory.getLogger(ClienteService.class);

  private static final String BASE_URL = "http://%s:%d/pessoa";
  private static final String TOKEN = "X-Auth-Token";
  private final RestTemplate rt = new RestTemplate();

  @Autowired
  private UsuarioService usuarioService;

  @Autowired
  private ClienteRepository repository;
  @Autowired
  private ConfiguracaoRepository cfgRepo;
  @Autowired
  private ObjectMapper om;

  @Override
  public String token() {
    return this.usuarioService.token();
  }

  @Override
  public ClienteRepository getRepository() {
    return repository;
  }

  @Override
  public void doAfterSave(Cliente obj) {
//    if (Optional.ofNullable(obj.getIdHabil()).orElse(0L) <= 0L) {
    PessoaDTO dto = this.enviaPessoa(obj);
    if (dto != null) {
      obj.setIdHabil(dto.getId());
      obj.setHasNewData(false);
    }
//    }
    this.repository.save(obj);
  }

  private String getToken() {
    return this.usuarioService.token();
  }


  private PessoaDTO enviaPessoa(Cliente cliente) {
    try {
      Configuracao cfg = cfgRepo.findFirstByOrderByIdDesc()
          .orElseThrow(() -> new EntityNotFoundException("configuracao not found"));
      PessoaDTO dto = Optional
          .ofNullable(fetchPessoa(cfg, cliente.getIdHabil()))
          .map(p -> p.fillFromCliente(cliente))
          .orElse(PessoaDTO.fromCliente(cliente));
      return enviaPessoaDTO(dto, cfg);
    } catch (IOException | URISyntaxException | EntityNotFoundException e) {
      LOGGER.error(e.getMessage(), e);
      return null;
    }
  }

  private HttpHeaders headers() {
    HttpHeaders headers = new HttpHeaders();
    headers.add(TOKEN, getToken());
    return headers;
  }

  private String formattedUrl(Configuracao cfg) {
    return String.format(BASE_URL, cfg.getHostH10(), cfg.getPortH10());
  }

  private PessoaDTO fetchPessoa(Configuracao cfg, Long idHabil) throws URISyntaxException {
    String url = this.formattedUrl(cfg) + "/" + idHabil;
    RequestEntity<Void> request = RequestEntity
        .get(new URI(url))
        .headers(headers())
        .build();
    ResponseEntity<PessoaDTO> response = this.rt.exchange(request, PessoaDTO.class);
    if (HttpStatus.OK.equals(response.getStatusCode())) {
      return response.getBody();
    } else {
      return null;
    }
  }

  private PessoaDTO enviaPessoaDTO(PessoaDTO pessoaDTO, Configuracao cfg) throws IOException, URISyntaxException {
    String url = this.formattedUrl(cfg) + "/save";
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    om.writeValue(out, pessoaDTO);
    HttpHeaders headers = headers();

    MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    params.add("obj", new String(out.toByteArray(), StandardCharsets.UTF_8));
    HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(params, headers);
    ResponseEntity<PessoaDTO> response = rt.postForEntity(new URI(url), request, PessoaDTO.class);
    if (response.getStatusCode().equals(HttpStatus.OK) || response.getStatusCode().equals(HttpStatus.CREATED)) {
      pessoaDTO.setId(Optional.ofNullable(response.getBody()).orElse(new PessoaDTO()).getId());
    }
    return pessoaDTO;
  }

  @Transactional
  public List<String> ufs() {
    List<String> ufs = new ArrayList<>();
    CriteriaBuilder builder = em().getCriteriaBuilder();
    CriteriaQuery<Tuple> query = builder.createTupleQuery();
    Root<Cliente> root = query.from(Cliente.class);
    query.multiselect(
        root.get("endereco").get("uf").alias("uf")
    );
    query.distinct(true);
    query.where(builder.isFalse(builder.coalesce(root.get("deleted"), false)));
    query.orderBy(builder.asc(root.get("endereco").get("uf")));
    List<Tuple> tuples = em().createQuery(query).getResultList();
    for (Tuple t : tuples) {
      String uf = t.get("uf", String.class);

      if (uf != null && !ufs.contains(uf)) {
        ufs.add(uf);
      }
    }
    return ufs;
  }

  public Boolean validateDocument(String document) {
    String doc = FormatUtils.removeNonAlphaNumericCharacters(document);
    Validator<String> validator;
    if (doc.length() <= 11) {
      validator = new CPFValidator(false);
    } else {
      validator = new CNPJValidator(false);
    }
    try {
      validator.assertValid(doc);
      return true;
    } catch (InvalidStateException ex) {
      return false;
    }
  }

  public boolean validateDocumentDoestNotExist(String document) throws DocumentConflictException {
    String doc = FormatUtils.removeNonAlphaNumericCharacters(document);
    doc = FormatUtils.getFormattedDoc(doc);
    if (this.repository.existsByCpfCnpj(doc)) {
      String msg = "Cliente com o documento " + doc + " já existe";
      throw new DocumentConflictException(msg);
    }
    return true;
  }

}
