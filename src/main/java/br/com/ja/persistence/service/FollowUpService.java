package br.com.ja.persistence.service;

import br.com.ja.dto.AbstractDTO;
import br.com.ja.dto.FollowUpDTO;
import br.com.ja.dto.pessoa.PessoaDTO;
import br.com.ja.model.FollowUp;
import br.com.ja.model.contatos.ContatoSimulacao;
import br.com.ja.model.juridico.Juridico;
import br.com.ja.persistence.repositories.FollowUpRepository;
import br.com.ja.persistence.repositories.JuridicoRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import static br.com.ja.persistence.service.ContatoSimulacaoService.X_AUTH_TOKEN;

@Service
public class FollowUpService extends AbstractService<FollowUp, FollowUpRepository> {

  private static final Logger LOG = Logger.getLogger(FollowUpService.class.getSimpleName());
  @Autowired
  private FollowUpRepository repository;
  @Autowired
  private ConfiguracaoService cfgService;
  @Autowired
  private UsuarioService usuarioService;
  @Autowired
  private ObjectMapper om;
  @Autowired
  private JuridicoRepository juridicoRepository;

  @Override
  public String token() {
    return this.usuarioService.token();
  }
  @Override
  public FollowUpRepository getRepository() {
    return repository;
  }

  @Override
  public void doAfterSave(FollowUp obj) {
    super.doAfterSave(obj);
    enviaServidorH10(obj);
    this.repository.save(obj);
  }


  private FollowUp enviaServidorH10(FollowUp obj) {
    try {
      Juridico juridico = this.juridicoRepository.getById(obj.getJuridico().getId());
      PessoaDTO funcionario = new PessoaDTO();
      ContatoSimulacao cs = juridico.getContato();
      funcionario.setId(cs.getVendedor() != null ? cs.getVendedor().getIdHabil() : 0L);
      AbstractDTO empresa = new AbstractDTO();
      PessoaDTO pessoa = new PessoaDTO();
      pessoa.setId(obj.getCliente() != null ? obj.getCliente().getIdHabil() : 0L);
      empresa.setId(obj.getIdEmpresa());
      FollowUpDTO dto = FollowUpDTO.builder()
          .motivo(obj.getMotivo())
          .empresa(empresa)
          .funcionario(funcionario)
          .tipo(obj.getTipo())
          .assunto(obj.getAssunto())
          .dataCadastro(obj.getDataHora())
          .resumoF(obj.getResumo())
          .build();
      if (pessoa.getId() > 0) {
        dto.setCliente(pessoa);
      }
      RestTemplate rt = new RestTemplate();
      String url = this.cfgService.api() + "/follow-up/save";
      HttpHeaders headers = new HttpHeaders();
      headers.add(X_AUTH_TOKEN, this.usuarioService.token());

      MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
      ByteArrayOutputStream out = new ByteArrayOutputStream();
      this.om.writeValue(out, dto);
      params.add("obj", new String(out.toByteArray(), StandardCharsets.UTF_8));
      HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(params, headers);
      ResponseEntity<FollowUpDTO> response = rt.postForEntity(new URI(url), request, FollowUpDTO.class);
      if (response.getStatusCode().equals(HttpStatus.OK) || response.getStatusCode().equals(HttpStatus.CREATED) && response.getBody() != null) {
        obj.setIdHabil(Optional.ofNullable(response.getBody()).orElse(new FollowUpDTO()).getId());
      }
    } catch (IOException | URISyntaxException | HttpServerErrorException | HttpClientErrorException e) {
      LOG.log(Level.SEVERE, "Erro ao enviar dados para o Hábil 10", e);
    }
    return obj;
  }


}
