package br.com.ja.schedulers.cliente;

import br.com.ja.model.cliente.Cliente;
import br.com.ja.model.cliente.ClienteTelefones;
import br.com.ja.persistence.sync.fetchers.cliente.ClienteDataFetcher;
import br.com.ja.persistence.sync.persisters.ja.cliente.ClienteDataPersister;
import br.com.ja.util.Timer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ClienteScheduler {

  private static final Logger LOGGER = LoggerFactory.getLogger(ClienteScheduler.class);

  private final ClienteDataFetcher fetcher;
  private final ClienteDataPersister persister;

  @Autowired
  public ClienteScheduler(ClienteDataFetcher fetcher, ClienteDataPersister persister) {
    this.fetcher = fetcher;
    this.persister = persister;
  }

  @Scheduled(fixedRate = 1000 * 60 * 5, initialDelay = 0L)
  public void doFetchNewData() {

    Timer.<Void>process(() -> {
      LOGGER.debug("doFechNewClientes::START");
      List<Cliente> clientes = this.fetcher.fetchClientes();
      LOGGER.debug("{} clientes to persist", clientes.size());
      this.persister.doPersistClientes(clientes);
      LOGGER.debug("doFechNewClientes::END");
      return null;
    }, "Download de Clientes");

    Timer.<Void>process(() -> {
      LOGGER.debug("doFechNewFones::START");
      List<ClienteTelefones> fones = this.fetcher.fetchClientesFones();
      LOGGER.debug("{} fones to persist", fones.size());
      this.persister.doPersistClientesFones(fones);
      LOGGER.debug("doFechNewFones::END");
      return null;
    }, "Download de Telefones");
  }

}
