package br.com.ja.schedulers.profissao;

import br.com.ja.model.Profissao;
import br.com.ja.persistence.sync.fetchers.profissao.ProfissaoDataFetcher;
import br.com.ja.persistence.sync.persisters.ja.profissao.ProfissaoDataPersister;
import br.com.ja.util.Timer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

//alter table profissao add constraint uq_id_habil_descricao unique (id_habil, descricao)

@Component
public class ProfissaoScheduler {

  private static final Logger LOGGER = LoggerFactory.getLogger(ProfissaoScheduler.class);

  private final ProfissaoDataPersister persister;
  private final ProfissaoDataFetcher fetcher;

  public ProfissaoScheduler(ProfissaoDataPersister persister, ProfissaoDataFetcher fetcher) {
    this.persister = persister;
    this.fetcher = fetcher;
  }

  @Scheduled(fixedRate = 1000 * 60 * 5, initialDelay = 0L)
  public void doFetchNewData() {
    LOGGER.debug("doFechNewProfissoes::START");
    Timer.<Void>process(() -> {

      List<Profissao> profissoes = this.fetcher.fetchProfissoes();
      LOGGER.debug("{} clientes to persist", profissoes.size());
      this.persister.doPersistProfissoes(profissoes);
      LOGGER.debug("doFechNewProfissoes::END");
      return null;
    }, "Download de Profissoes");
  }
}
