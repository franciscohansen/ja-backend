package br.com.ja.model.cliente;

import br.com.ja.model.AbstractModel;
import br.com.ja.model.Endereco;
import br.com.ja.model.Profissao;
import br.com.ja.model.enums.EEstadoCivil;
import br.com.ja.model.enums.ESexo;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Builder
public class Cliente extends AbstractModel {

  @Column(columnDefinition = "BOOLEAN DEFAULT FALSE")
  private boolean pessoaJuridica;
  private String nome;
  private String razaoSocial;
  private String cpfCnpj;
  private String rgIe;
  @Temporal(TemporalType.TIMESTAMP)
  private Date dataNascimento;
  private String email;
  @Enumerated(EnumType.STRING)
  private EEstadoCivil estadoCivil;
  private String naturalidade;
  @Embedded
  private Endereco endereco;
  @Embedded
  private Endereco enderecoCarne;

  private String nomePai;
  private String nomeMae;

  @ManyToOne
  @JoinColumn(name = "id_profissao")
  private Profissao profissao;
  @Enumerated(EnumType.STRING)
  private ESexo sexo;
  @OneToMany(orphanRemoval = true, mappedBy = "cliente")
  private List<ClienteTelefones> telefones;

  private String banco;
  private String agencia;
  private String contaBancaria;
  @OneToMany(orphanRemoval = true, mappedBy = "cliente")
  private List<ClienteVeiculo> veiculos;

  @Column(columnDefinition = "BOOLEAN DEFAULT FALSE")
  private Boolean hasNewData;


  @Override
  public void corrigeRelacoes() {
    Optional.ofNullable(veiculos)
        .ifPresent(veic -> veic.forEach(v -> v.setCliente(this)));
  }
}
