package br.com.ja.model.cliente;

import br.com.ja.model.AbstractModel;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ClienteTelefones extends AbstractModel {

  @ManyToOne
  @JoinColumn(name = "id_cliente", referencedColumnName = "id")
  @JsonBackReference
  private Cliente cliente;

  @Column(columnDefinition = "BOOLEAN DEFAULT FALSE")
  private Boolean padrao;
  private String tipo;
  private String ddd;
  private String fone;
  private String ramal;
  private String observacoes;

  @Override
  public String toString() {
    return "ClienteTelefones{" +
        "id=" + getId() +
        ", observacoes='" + observacoes + '\'' +
        ", padrao=" + padrao +
        ", tipo='" + tipo + '\'' +
        ", ddd='" + ddd + '\'' +
        ", fone='" + fone + '\'' +
        ", ramal='" + ramal + '\'' +
        ", id_habil=" + getIdHabil() +
        ", cliente=" + cliente +
        '}';
  }
}
