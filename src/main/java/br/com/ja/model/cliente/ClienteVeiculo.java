package br.com.ja.model.cliente;

import br.com.ja.model.AbstractModel;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ClienteVeiculo extends AbstractModel {

  @ManyToOne(optional = false)
  @JoinColumn(name = "id_cliente", referencedColumnName = "id")
  private Cliente cliente;

  private String nomeCondutor;
  private String cpfCondutor;
  private String marca;
  private String modelo;
  private String cor;
  private String anoModelo;
  private String placa;
  private String renavam;
  private String chassi;
  private String crlvAno;
  private String cnh;
  private String ano;


}
