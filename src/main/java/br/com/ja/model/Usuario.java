package br.com.ja.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;
import net.minidev.json.annotate.JsonIgnore;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@EqualsAndHashCode(callSuper = true)
public class Usuario extends AbstractModel {

  private String nome;

  private String login;

  @JsonDeserialize
  @JsonIgnore
  @Lob
  @Type(type = "text")
  private String token;

  @Column(columnDefinition = "BOOLEAN DEFAULT FALSE")
  private boolean administrador;
  @Column(columnDefinition = "BOOLEAN DEFAULT FALSE")
  private Boolean acessaVendas;
  @Column(columnDefinition = "BOOLEAN DEFAULT FALSE")
  private Boolean acessaJuridico;
  @Column(columnDefinition = "BOOLEAN DEFAULT FALSE")
  private Boolean acessaNegociacoes;

}
