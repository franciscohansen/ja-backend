package br.com.ja.model.enums;

public enum EEstadoCivil {
  NAO_SE_APLICA("Não Definido", ""),
  AMASIADO("Amasiado(a)", "A"),
  CASADO("Casado(a)", "C"),
  DESQUITADO("Desquitado(a)", "D"),
  OUTRO("Outro", "O"),
  SOLTEIRO("Solteiro(a)", "S"),
  VIUVO("Viúvo(a)", "V");
  private final String descricao;
  private final String valorHE;

  EEstadoCivil(String descricao, String valorHE) {
    this.descricao = descricao;
    this.valorHE = valorHE;
  }
}
