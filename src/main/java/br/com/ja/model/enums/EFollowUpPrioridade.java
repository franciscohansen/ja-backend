package br.com.ja.model.enums;

public enum EFollowUpPrioridade {
  NENHUM,
  BAIXA,
  MEDIA,
  ALTA,
  URGENTE;
}
