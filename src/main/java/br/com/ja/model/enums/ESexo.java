package br.com.ja.model.enums;

public enum ESexo {
  MASCULINO,
  FEMININO,
  NAO_SE_APLICA
}
