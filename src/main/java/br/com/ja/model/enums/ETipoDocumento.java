package br.com.ja.model.enums;

public enum ETipoDocumento {
  VEICULO,
  CARTAO_CREDITO,
  EMPRESTIMOS
}
