package br.com.ja.model;

import lombok.*;

import javax.persistence.Entity;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Configuracao extends AbstractModel {

  private double comissaoPadrao;
  private double percentualQuitacao;
  private String hostH10;
  private int portH10;
}
