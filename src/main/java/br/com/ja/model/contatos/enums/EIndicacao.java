package br.com.ja.model.contatos.enums;

public enum EIndicacao {
  NENHUM(""),
  CLIENTE("Indicação de Cliente"),
  NAO_CLIENTE("Indicação Não Cliente"),
  FACEBOOK("Facebook"),
  TWITTER("Twitter"),
  INSTAGRAM("Instagram"),
  GOOGLE("Google"),
  JORNAL("Jornal"),
  TV("TV"),
  SBT("SBT"),
  REDETV("RedeTV"),
  GLOBO("Globo"),
  RECORD("Record"),
  REVISTA("Revista"),
  OUTRO("Outro");
  private final String descricao;

  EIndicacao(String descricao) {
    this.descricao = descricao;
  }
}
