package br.com.ja.model.contatos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Embeddable
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Adesao implements Serializable {

  private Double valor;
  @Temporal(TemporalType.TIMESTAMP)
  private Date primeiroVencimento;
  private Integer diasEntre;
  private Integer nroParcelas;

}
