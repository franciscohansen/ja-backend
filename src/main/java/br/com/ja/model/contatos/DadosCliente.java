package br.com.ja.model.contatos;

import br.com.ja.model.Endereco;
import br.com.ja.model.Profissao;
import br.com.ja.model.cliente.Cliente;
import br.com.ja.model.enums.EEstadoCivil;
import br.com.ja.model.enums.ESexo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Embeddable
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DadosCliente implements Serializable {

  @Column(columnDefinition = "BOOLEAN DEFAULT FALSE")
  private Boolean novo;

  @ManyToOne
  @JoinColumn(name = "id_cliente", referencedColumnName = "id")
  private Cliente cliente;
  private String nome;
  private String razaoSocial;
  private String cpfCnpj;
  private String rgIe;
  private String email;
  @Temporal(TemporalType.TIMESTAMP)
  private Date dataNascimento;
  @Column(columnDefinition = "BOOLEAN DEFAULT FALSE")
  private Boolean pessoaJuridica;
  @Enumerated(EnumType.STRING)
  private ESexo sexo;
  @Enumerated(EnumType.STRING)
  private EEstadoCivil estadoCivil;
  @Embedded
  private Endereco endereco;
  @Embedded
  private Endereco enderecoCarne;
  private String naturalidade;

  private String nomePai;
  private String nomeMae;

  @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "id_contato")
  private List<DadosClienteTelefone> telefones = new ArrayList<>();

  @ManyToOne
  @JoinColumn(name = "id_profissao")
  private Profissao profissao;

  public List<DadosClienteTelefone> getTelefones() {
    if (this.telefones == null) {
      this.telefones = new ArrayList<>();
    }
    return telefones;
  }

  public void setTelefones(List<DadosClienteTelefone> telefones) {
    getTelefones().clear();
    if (telefones != null) {
      this.telefones.addAll(telefones);
    }
  }
}
