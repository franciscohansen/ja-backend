package br.com.ja.model.contatos;

import br.com.ja.model.cliente.Cliente;
import br.com.ja.model.contatos.enums.EIndicacao;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;


@Data
@Builder
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class Indicacao implements Serializable {
  @Enumerated(EnumType.STRING)
  private EIndicacao indicacao;
  @ManyToOne
  @JoinColumn(name = "id_cliente_indicador", referencedColumnName = "id")
  private Cliente cliente;
  private String nome;
  private String cpfCnpj;
  private String rgIe;
  private String dadosBancarios;
  private String banco;
  private String agencia;
  private String contaBancaria;
  private String pix;

}
