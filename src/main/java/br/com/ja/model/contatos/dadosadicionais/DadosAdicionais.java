package br.com.ja.model.contatos.dadosadicionais;

import br.com.ja.model.AbstractModel;
import br.com.ja.model.contatos.ContatoSimulacao;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@Table(name = "cs_dados_adicionais")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class DadosAdicionais extends AbstractModel {

  @OneToOne(optional = false)
  @JoinColumn(name = "id_contato", referencedColumnName = "id")
  @JsonBackReference
  private ContatoSimulacao contato;

  @Embedded
  @Builder.Default
  private Veiculo veiculo = new Veiculo();
  @Embedded
  @Builder.Default
  private Cartao cartao = new Cartao();
  @Embedded
  @Builder.Default
  private Emprestimo emprestimo = new Emprestimo();
  @Lob
  @Type(type = "text")
  @Column(name = "OBSERVACOES")
  private String observacoes;

  @JsonInclude
  @JsonProperty("veiculo")
  public Veiculo getVeiculo() {
    if (veiculo == null) {
      veiculo = new Veiculo();
    }
    return veiculo;
  }

  @JsonInclude
  @JsonProperty("cartao")
  public Cartao getCartao() {
    if (cartao == null) {
      cartao = new Cartao();
    }
    return cartao;
  }

  @JsonInclude
  @JsonProperty("emprestimo")
  public Emprestimo getEmprestimo() {
    if (emprestimo == null) {
      emprestimo = new Emprestimo();
    }
    return emprestimo;
  }
}
