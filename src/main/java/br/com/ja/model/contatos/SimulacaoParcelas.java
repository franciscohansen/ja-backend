package br.com.ja.model.contatos;

import br.com.ja.model.AbstractModel;
import br.com.ja.model.contatos.enums.ETipoParcela;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "cs_parcelas")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SimulacaoParcelas extends AbstractModel {

  @ManyToOne(optional = false)
  @JoinColumn(name = "id_contato", referencedColumnName = "id")
  @JsonBackReference
  private ContatoSimulacao contato;
  private String documento;
  @Temporal(TemporalType.TIMESTAMP)
  private Date vencimento;
  private double valor;
  @Enumerated(EnumType.STRING)
  private ETipoParcela tipo;
}

