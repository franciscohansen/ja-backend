package br.com.ja.model.contatos;

import br.com.ja.model.AbstractModel;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "cs_dados_telefone")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class DadosClienteTelefone extends AbstractModel {

  @Column(columnDefinition = "BOOLEAN DEFAULT FALSE")
  private Boolean padrao;
  private String tipo;
  private String ddd;
  private String fone;
  private String ramal;
  private String observacoes;
}
