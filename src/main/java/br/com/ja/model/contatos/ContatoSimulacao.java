package br.com.ja.model.contatos;

import br.com.ja.model.AbstractModelComUsuario;
import br.com.ja.model.Funcionario;
import br.com.ja.model.contatos.dadosadicionais.DadosAdicionais;
import br.com.ja.model.contatos.enums.EStatusContato;
import br.com.ja.model.contatos.enums.ETipoContrato;
import br.com.ja.model.contatos.procurador.Procurador;
import br.com.ja.model.juridico.Juridico;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Builder
public class ContatoSimulacao extends AbstractModelComUsuario {
  @Column(columnDefinition = "BOOLEAN DEFAULT FALSE")
  private Boolean inativo;

  @Temporal(TemporalType.TIMESTAMP)
  private Date dataLcto;

  @Embedded
  private Indicacao dadosIndicacao;
  @Enumerated(EnumType.STRING)
  private ETipoContrato tipoContrato;
  @ManyToOne
  @JoinColumn(name = "id_vendedor", referencedColumnName = "id")
  private Funcionario vendedor;
  @Embedded
  private DadosCliente dadosCliente;
  @Embedded
  private Adesao adesao;
  @Enumerated(EnumType.STRING)
  private EStatusContato status;
  private Double comissao;
  @Column(columnDefinition = "BOOLEAN DEFAULT FALSE")
  private Boolean comProcurador;
  @Column(columnDefinition = "BOOLEAN DEFAULT FALSE")
  private Boolean cancelado;

  @OneToOne(mappedBy = "contato", cascade = CascadeType.ALL)
  private DadosAdicionais dadosAdicionais;
  @OneToOne(mappedBy = "contato", cascade = CascadeType.ALL)
  private Procurador procurador;

  @OneToOne(mappedBy = "contato", cascade = CascadeType.ALL)
  private Simulacao simulacao;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "contato")
  @Builder.Default
  private List<SimulacaoDocumentos> documentos = new ArrayList<>();

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "contato")
  @Builder.Default
  private List<SimulacaoParcelas> parcelas = new ArrayList<>();


  @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "contato")
  private Juridico juridico;

  private Long idVendaParcela;
  private Long refVendaParcela;

  private Long idVendaAdesao;
  private Long refVendaAdesao;

  @Temporal(TemporalType.TIMESTAMP)
  private Date dataVendaParcela;
  @Temporal(TemporalType.TIMESTAMP)
  private Date dataVendaAdesao;



  @Override
  public void corrigeRelacoes() {
    super.corrigeRelacoes();
    if (this.dadosAdicionais != null) {
      if (Optional.ofNullable(this.dadosAdicionais.getId()).orElse(0L) == 0L) {
        this.dadosAdicionais.setId(null);
      }
      this.dadosAdicionais.setContato(this);
      this.dadosAdicionais.corrigeRelacoes();
    }
    if (this.procurador != null) {
      if (Optional.ofNullable(this.procurador.getId()).orElse(0L) == 0L) {
        this.procurador.setId(null);
      }
      this.procurador.setContato(this);
      this.procurador.corrigeRelacoes();
    }
    if (this.simulacao != null) {
      if (Optional.ofNullable(this.simulacao.getId()).orElse(0L) == 0L) {
        this.simulacao.setId(null);
      }
      this.simulacao.setContato(this);
      this.simulacao.corrigeRelacoes();
    }
    if (this.documentos != null) {
      for (SimulacaoDocumentos sd : this.documentos) {
        if (Optional.ofNullable(sd.getId()).orElse(0L) == 0L) {
          sd.setId(null);
        }
        sd.setContato(this);
      }
    }
    if (this.parcelas != null) {
      for (SimulacaoParcelas p : this.parcelas) {
        if (Optional.ofNullable(p.getId()).orElse(0L) == 0L) {
          p.setId(null);
        }
        p.setContato(this);
        p.corrigeRelacoes();
      }
    }
    if (this.juridico != null) {
      if (Optional.ofNullable(this.juridico.getId()).orElse(0L) == 0L) {
        this.juridico.setId(null);
      }
      this.juridico.setContato(this);
      this.juridico.corrigeRelacoes();
    }
  }
}
