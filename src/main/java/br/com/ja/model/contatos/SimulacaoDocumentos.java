package br.com.ja.model.contatos;

import br.com.ja.model.AbstractModel;
import br.com.ja.model.Documento;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "cs_documentos")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@EqualsAndHashCode(callSuper = true)
public class SimulacaoDocumentos extends AbstractModel {

  @ManyToOne(optional = false)
  @JoinColumn(name = "id_contato", referencedColumnName = "id")
  @JsonBackReference
  private ContatoSimulacao contato;

  @ManyToOne
  @JoinColumn(name = "id_documento", referencedColumnName = "id")
  private Documento documento;

  private String descDocumento;

  @Column(columnDefinition = "BOOLEAN DEFAULT FALSE")
  private Boolean entregue;
}
