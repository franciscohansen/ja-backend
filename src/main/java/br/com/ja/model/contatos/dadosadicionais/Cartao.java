package br.com.ja.model.contatos.dadosadicionais;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class Cartao  implements Serializable {
  @Temporal(TemporalType.TIMESTAMP)
  private Date ultimaFatura;
  private String bandeira;
  private String nroContrato;

}
