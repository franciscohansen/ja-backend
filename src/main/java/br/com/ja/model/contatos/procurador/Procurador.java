package br.com.ja.model.contatos.procurador;

import br.com.ja.model.AbstractModel;
import br.com.ja.model.Endereco;
import br.com.ja.model.contatos.ContatoSimulacao;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "cs_procurador")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Procurador extends AbstractModel {
  @OneToOne(optional = false)
  @JoinColumn(name = "id_contato", referencedColumnName = "id")
  @JsonBackReference
  private ContatoSimulacao contato;

  private String nome;
  private String nacionalidade;
  private String naturalidade;
  private String profissao;
  private String rg;
  private String cpf;
  private String filiacao;
  @Embedded
  private Endereco endereco;
  private String email;
  private String fone;

}
