package br.com.ja.model.contatos.enums;

public enum EStatusContato {
  PENDENTE,
  VENDIDO,
}
