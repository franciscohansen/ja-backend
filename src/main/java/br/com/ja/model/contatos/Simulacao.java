package br.com.ja.model.contatos;

import br.com.ja.model.AbstractModel;
import br.com.ja.model.Banco;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "cs_simulacao")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Simulacao extends AbstractModel {

  @OneToOne(optional = false)
  @JoinColumn(name = "id_contato", referencedColumnName = "id")
  @JsonBackReference
  private ContatoSimulacao contato;

  @ManyToOne
  @JoinColumn(name = "id_banco", referencedColumnName = "id")
  private Banco banco;

  private Double perc;
  private Double valorFinanciado;
  private Integer totalParcelas;
  private Double valorParcela;
  private Integer parcelasPagas;
  private Integer parcelasRestantes;
  private Double saldoPagar;
  private Double renValorParcela;
  private Double renSaldo;
  private Integer renNroParcelas;
  @Temporal(TemporalType.TIMESTAMP)
  private Date dataVencimento;
  private Integer diasEntreParcelas;
  @Temporal(TemporalType.TIMESTAMP)
  private Date previsaoQuitacao;
  private String nroContrato;
  private Integer parcelasAtrasadas;
  private String bancoPrazo;
  private Integer mesesQuitacao;


}
