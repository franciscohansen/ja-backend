package br.com.ja.model.contatos.enums;

public enum ETipoContrato {
  VEICULO("Veículos"),
  CARTAO_CREDITO("Cartão de Crédito"),
  EMPRESTIMOS("Empréstimos"),
  OUTROS("Outros"),
  GERAL("Geral");
  private final String descricao;

  ETipoContrato(String descricao) {
    this.descricao = descricao;
  }
}
