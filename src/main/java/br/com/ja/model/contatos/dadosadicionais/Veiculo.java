package br.com.ja.model.contatos.dadosadicionais;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@Builder
@Embeddable
@AllArgsConstructor
@NoArgsConstructor
public class Veiculo implements Serializable {
  private String nomeCondutor;
  private String cpfCondutor;
  private String marca;
  private String modelo;
  private String cor;
  private String anoModelo;
  private String placa;
  private String renavam;
  private String chassi;
  private String crlvAno;
  private String cnh;
  private String ano;

}
