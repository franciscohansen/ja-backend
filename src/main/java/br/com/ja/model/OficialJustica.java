package br.com.ja.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class OficialJustica extends AbstractModel {
  private String nome;
  private String telefone;
  private String email;
  @ManyToOne
  @JoinColumn(name = "id_forum", referencedColumnName = "id")
  private Forum forum;
}
