package br.com.ja.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@MappedSuperclass
public abstract class AbstractModel implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private Long idEmpresa;

  private Long idHabil;

  @Column(columnDefinition = "BOOLEAN DEFAULT FALSE")
  private Boolean deleted = false;

  public void corrigeRelacoes() {

  }
}
