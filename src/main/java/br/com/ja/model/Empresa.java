package br.com.ja.model;

import lombok.*;

import javax.persistence.Entity;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Empresa extends AbstractModel {
  private String nome;
  private String cpfCnpj;
}
