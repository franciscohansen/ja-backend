package br.com.ja.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
@EqualsAndHashCode(callSuper = true)
public abstract class AbstractModelComUsuario extends AbstractModel {
  @ManyToOne
  @JoinColumn(name = "id_usuario")
  private Usuario usuario;
}
