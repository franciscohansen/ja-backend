package br.com.ja.model;

import br.com.ja.model.cliente.Cliente;
import br.com.ja.model.enums.EFollowUpPrioridade;
import br.com.ja.model.juridico.Juridico;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class FollowUp extends AbstractModel {
  @ManyToOne
  @JoinColumn(name = "id_juridico", referencedColumnName = "id")
  @JsonBackReference
  private Juridico juridico;

  @ManyToOne
  @JoinColumn(name = "id_cliente", referencedColumnName = "id")
  private Cliente cliente;

  @ManyToOne
  @JoinColumn(name = "id_funcionario", referencedColumnName = "id")
  private Funcionario funcionario;

  private String motivo;
  private String assunto;
  @Temporal(TemporalType.TIMESTAMP)
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
  private Date dataHora;
  @Lob
  @Type(type = "text")
  private String resumo;
  @Temporal(TemporalType.TIMESTAMP)
  @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
  private Date proximoContato;
  @Enumerated(EnumType.STRING)
  private EFollowUpPrioridade prioridade;
  private String proximoContatoHora;
  private Long idAgenda;
  private String tipo;
  private String textoEmail;
}
