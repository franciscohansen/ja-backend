package br.com.ja.model;

import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Lob;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Forum extends AbstractModel {
  private String nome;
  @Embedded
  private Endereco endereco;
  @Lob
  @Type(type = "text")
  private String observacoes;
  private String telefone;
}
