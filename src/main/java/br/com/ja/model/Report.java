package br.com.ja.model;

import br.com.ja.model.enums.EPrintContext;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Report extends AbstractModel {

  private String description;
  private String path;
  @Enumerated(EnumType.STRING)
  private EPrintContext contexto;
}
