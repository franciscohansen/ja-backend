package br.com.ja.model;

import br.com.ja.model.enums.ETipoDocumento;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Entity
@Table(name = "JA_DOCUMENTO")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Documento extends AbstractModel {
  private String descricao;
  @Enumerated(EnumType.STRING)
  private ETipoDocumento tipo;
}
