package br.com.ja.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Endereco implements Serializable {
  private String logradouro;
  private String cep;
  private String numero;
  private String bairro;
  private String municipio;
  private String uf;
  private String complemento;
  private String pais;
}
