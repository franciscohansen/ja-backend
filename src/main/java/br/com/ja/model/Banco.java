package br.com.ja.model;

import lombok.*;

import javax.persistence.Entity;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Banco extends AbstractModel {

  private String descricao;

  private String prazo;

  private double perc;

}
