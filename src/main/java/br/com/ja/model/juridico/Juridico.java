package br.com.ja.model.juridico;

import br.com.ja.model.AbstractModel;
import br.com.ja.model.contatos.ContatoSimulacao;
import br.com.ja.model.juridico.situacao.JuridicoSituacao;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Juridico extends AbstractModel {
  @OneToOne
  @JoinColumn(name = "id_contato", referencedColumnName = "id")
  @JsonBackReference
  @NotFound(action = NotFoundAction.IGNORE)
  private ContatoSimulacao contato;

  @Column(columnDefinition = "BOOLEAN DEFAULT FALSE")
  private Boolean cadastroPush;

  @OneToMany(orphanRemoval = true, mappedBy = "juridico", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @Builder.Default
  private Set<JuridicoLinks> links = new HashSet<>();

  @OneToMany(orphanRemoval = true, mappedBy = "juridico", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
  @Builder.Default
  private List<JuridicoSituacao> situacoes = new ArrayList<>();

  @OneToMany(orphanRemoval = true, mappedBy = "juridico", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @Builder.Default
  private List<JuridicoVisualizacao> visualizacoes = new ArrayList<>();
  @Lob
  @Type(type = "text")
  private String observacoes;


  @Override
  public void corrigeRelacoes() {
    super.corrigeRelacoes();
    if (links != null) {
      for (JuridicoLinks link : links) {
        link.setJuridico(this);
      }
    }
    if (situacoes != null) {
      for (JuridicoSituacao situacao : situacoes) {
        situacao.setJuridico(this);
      }
    }
    if (visualizacoes != null) {
      for (JuridicoVisualizacao jv : visualizacoes) {
        jv.setJuridico(this);
      }
    }
  }
}
