package br.com.ja.model.juridico.situacao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class JuridicoBusca implements Serializable {
  private Boolean aguardandoLiberacao;
  private Boolean arquivado;
  private String classeProcessual;
  private String nroProcesso;


}
