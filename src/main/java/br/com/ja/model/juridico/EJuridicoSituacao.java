package br.com.ja.model.juridico;

public enum EJuridicoSituacao {
  SEM_BUSCA,
  MANDADO_EXPEDIDO,
  COM_BUSCA,
  CONTRATO_CANCELADO,
  CARRO_APREENDIDO,
  RENAJUD,
  EM_EXECUCAO,
  FINALIZADO,
  ARQUIVADO
}
