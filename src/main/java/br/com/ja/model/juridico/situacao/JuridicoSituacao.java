package br.com.ja.model.juridico.situacao;

import br.com.ja.model.AbstractModel;
import br.com.ja.model.Forum;
import br.com.ja.model.OficialJustica;
import br.com.ja.model.juridico.EJuridicoSituacao;
import br.com.ja.model.juridico.Juridico;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class JuridicoSituacao extends AbstractModel {

  @ManyToOne(optional = false)
  @JoinColumn(name = "id_juridico", referencedColumnName = "id")
  @JsonBackReference
  private Juridico juridico;

  @Enumerated(EnumType.STRING)
  private EJuridicoSituacao situacao;
  @Temporal(TemporalType.TIMESTAMP)
  private Date data;

  @Column(columnDefinition = "BOOLEAN DEFAULT FALSE")
  private Boolean ativo;

  @ManyToOne(optional = true)
  @JoinColumn(name = "id_forum", referencedColumnName = "id")
  private Forum forum;
  @ManyToOne(optional = true)
  @JoinColumn(name = "id_oficial_justica", referencedColumnName = "id")
  private OficialJustica oficialJustica;
  @Embedded
  private JuridicoBusca busca;
  @Embedded
  private JuridicoCarroApreendido carroApreendido;
  @Embedded
  private JuridicoExecucao execucao;
  @Embedded
  private JuridicoMandado mandado;
  @Embedded
  private JuridicoRenajud renajud;
  @Lob
  @Type(type = "text")
  private String observacoes;


}
