package br.com.ja.model.juridico;

import br.com.ja.model.AbstractModel;
import br.com.ja.model.Usuario;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class JuridicoVisualizacao extends AbstractModel {

  @ManyToOne(optional = false)
  @JoinColumn(name = "id_juridico", referencedColumnName = "id")
  @JsonBackReference
  private Juridico juridico;
  @Temporal(TemporalType.TIMESTAMP)
  private Date data;

  private Integer nroVisualizacoes;
  @ManyToOne
  @JoinColumn(name = "id_usuario", referencedColumnName = "id")
  private Usuario usuario;
}
