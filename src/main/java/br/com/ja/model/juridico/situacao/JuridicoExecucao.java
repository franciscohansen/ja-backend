package br.com.ja.model.juridico.situacao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JuridicoExecucao implements Serializable {

  @Column(columnDefinition = "BOOLEAN DEFAULT FALSE")
  private Boolean bacenjud;

}
