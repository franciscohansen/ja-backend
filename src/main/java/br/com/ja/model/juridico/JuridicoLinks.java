package br.com.ja.model.juridico;

import br.com.ja.model.AbstractModel;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class JuridicoLinks extends AbstractModel {
  @ManyToOne
  @JoinColumn(name = "id_juridico", referencedColumnName = "id")
  @JsonBackReference
  private Juridico juridico;
  private String link;
}
