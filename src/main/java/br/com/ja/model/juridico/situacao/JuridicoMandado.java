package br.com.ja.model.juridico.situacao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JuridicoMandado implements Serializable {
  private String nro;
  private Integer idDecisao;
  private String resumoDecisao;


}
