package br.com.ja.model.juridico.situacao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.Embeddable;
import javax.persistence.Lob;
import java.io.Serializable;

@Embeddable
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JuridicoRenajud implements Serializable {

  private Integer idDecisao;
  @Lob
  @Type(type = "text")
  private String observacoes;
}
