package br.com.ja.projections;

public interface FollowUpMotivoProjection {
  String getMotivo();
}
